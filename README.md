# CodeGenerator
Generador de codigo Java a partir de lenguaje cotidiano

**Especificaciones**
<br />A  partir  del código  fuente  de la  suma,  de la  función  repetir  y negación,  generar  las siguientes operaciones:<br />
· Resta<br />
· Multiplicación<br />
· División<br />
· Potencia<br />
· Factorial<br />
· Porcentaje<br />
· Media aritmética<br />
· Raíz cuadrada<br />

Todas  las operaciones deben  poder  ser  reutilizadas  después  de  ser creadas por  el  generador para  crear  nuevas operaciones. De  tal  forma  que para  crear  la  potencia  primero  debemos crear la multiplicación y luego utilizarla. Se  pueden  crear  todas  las  operaciones  que  se  consideren  necesarias  siempre  que  estén creadas con el generador. La función repetir puede dividirse en varias funciones si así se desea. Generador Entrada de usuario Código generado Operaciones básicas 
<br /><br />**Condiciones**
<br />Están  prohibidas  las  funciones  de  librerías  propias  del  lenguaje  elegido.  Por  ejemplo,  las funciones “pow” para la potencia o “sqrt” para la raíz cuadrada. Se deben utilizar las funciones escritas por el alumno o generadas. El generador  debe permitir al usuario introducir el nombre de la operación que desea  crear y los  parámetros  que recibe,  así  como su  definición.  No  se  puede hardcodear  la  generación  del código, es decir, el usuario tiene que indicar al generador que es cada operación. El  código  generado  tiene  que  ser  compilable  o  no  contener  errores  de  sintaxis  en  el  caso  de elegir un lenguaje interpretado.

Participantes:
- Daniel Alcalá Valera
- Martín Bayón Gutiérrez
- Beatriz Crespo Torbado