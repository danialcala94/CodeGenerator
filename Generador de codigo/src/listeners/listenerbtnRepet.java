package listeners;

import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ventanas.VentanaRepeticion;

public class listenerbtnRepet implements ActionListener{

	ventanas.VentanaPrincipal ventanaPrincipal;
	
	public listenerbtnRepet(ventanas.VentanaPrincipal ventanaPrincipal) {
		this.ventanaPrincipal = ventanaPrincipal;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		VentanaRepeticion ventanaRepe = new VentanaRepeticion(Integer.parseInt(ventanaPrincipal.getTextField().getText()),ventanaPrincipal.getArrayMetodos(), ventanaPrincipal.getArrayParametros(), ventanaPrincipal.getArrayVariable());
		ventanaRepe.show();
		TextArea texto = ventanaPrincipal.getTextArea();
		if(ventanaRepe.repeticion != null){
			texto.setText(texto.getText() + ventanaRepe.repeticion);
		}
	}

}