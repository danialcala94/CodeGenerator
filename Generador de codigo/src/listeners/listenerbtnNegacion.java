package listeners;

import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import objetos.Metodo;
import objetos.Parametro;
import objetos.Variable;
import ventanas.VentanaNegacion;
import ventanas.VentanaPrincipal;

public class listenerbtnNegacion implements ActionListener {
	
	VentanaPrincipal ventanaPrincipal;
	
	public listenerbtnNegacion(VentanaPrincipal ventanaPrincipal, ArrayList<Metodo> metodos, ArrayList<Parametro> parametros, ArrayList<Variable> variables) {
		this.ventanaPrincipal = ventanaPrincipal;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		VentanaNegacion ventana = new VentanaNegacion(Integer.parseInt(ventanaPrincipal.getTextField().getText().toString()), ventanaPrincipal.getArrayMetodos(), ventanaPrincipal.getArrayParametros(), ventanaPrincipal.getArrayVariable());
		ventana.show(true);
		ventana.setModal(true);
		if(ventana.resultado!=null){
			TextArea texto = ventanaPrincipal.getTextArea();
			texto.setText(texto.getText() + ventana.resultado+";\n");
		}
	
	}

}