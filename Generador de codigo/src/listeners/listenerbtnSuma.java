package listeners;

import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import ventanas.VentanaSuma;

public class listenerbtnSuma implements ActionListener{

	
	ventanas.VentanaPrincipal ventanaPrincipal;
	ventanas.VentanaSuma ventSuma;
	
	public listenerbtnSuma(ventanas.VentanaPrincipal ventanaPrincipal) {
		this.ventanaPrincipal = ventanaPrincipal;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// Accion que se realizará al pulsar el boton de suma
		ventSuma = new VentanaSuma(Integer.parseInt(ventanaPrincipal.getTextField().getText().toString()),ventanaPrincipal.getArrayMetodos(), ventanaPrincipal.getArrayParametros(), ventanaPrincipal.getArrayVariable());	//Modificar este numero para que lea el campo de la ventana Principal
		ventSuma.show();
		if(ventSuma.resultado!=null){
			TextArea texto = ventanaPrincipal.getTextArea();
			texto.setText(texto.getText() + ventSuma.resultado+";\n");
		}
	}

}