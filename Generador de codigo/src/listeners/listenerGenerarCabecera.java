package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class listenerGenerarCabecera implements ActionListener{

	ventanas.VentanaPrincipal ventanaPrincipal;
	
	public listenerGenerarCabecera(ventanas.VentanaPrincipal ventanaPrincipal) {
		this.ventanaPrincipal = ventanaPrincipal;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		//Este metodo se ejecuta al pulsar el boton generar cabecera de la ventana principal
		ventanaPrincipal.getComboBox();
	}
}
