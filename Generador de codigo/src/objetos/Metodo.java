package objetos;

import java.util.ArrayList;

/**
 * Representa a una determinada <strong>operaci�n</strong> (m�todo) implementada.
 * <strong><ul><li>(int) numArgumentos</li><li>(String) codigo</li></ul></strong>
 * @author <ul><li>Daniel Alcal� Valera</li><li>Mart�n Bay�n Guti�rrez</li><li>Beatriz Crespo Torbado</li></ul>
 *
 */
public class Metodo {
	
	private String nombre = "";
	private int numParametros = 0;
	private String codigo = "";
	private boolean isOriginal = false;
	private ArrayList<Parametro> parametros;
	
	/**
	 * Constructor de m�todo sin argumentos ni c�digo.
	 * <strong><ul><li>(int) numArgumentos = 0</li><li>(String) codigo = ""</li></ul></strong>
	 */
	public Metodo() {
		this.nombre = "M�todo";
		this.numParametros = 0;
		this.codigo = "";
		this.isOriginal = false;
		this.parametros = new ArrayList<Parametro>();
	}
	
	public Metodo(String nombre) {
		this.nombre = nombre;
		this.numParametros = 0;
		this.codigo = "";
		this.isOriginal = false;
		this.parametros = new ArrayList<Parametro>();
	}
	
	public Metodo(String nombre, boolean isOriginal) {
		this.nombre = nombre;
		this.numParametros = 0;
		this.codigo = "";
		this.isOriginal = isOriginal;
		this.parametros = new ArrayList<Parametro>();
	}
	
	/**
	 * Constructor de m�todo con argumentos y c�digo.
	 * <strong><ul><li>(int) numArgumentos > 0</li><li>(String) codigo != ""</li></ul></strong>
	 */
	public Metodo(String nombre, int numArgumentos, String codigo) {
		this.nombre = nombre;
		this.numParametros = numArgumentos;
		this.codigo = codigo;
		this.isOriginal = false;
		this.parametros = new ArrayList<Parametro>();
	}
	
	public Metodo(String nombre, int numArgumentos, String codigo, boolean isOriginal) {
		this.nombre = nombre;
		this.numParametros = numArgumentos;
		this.codigo = codigo;
		this.isOriginal = isOriginal;
		this.parametros = new ArrayList<Parametro>();
	}
	
	public Metodo(String nombre, int numArgumentos, String codigo, boolean isOriginal, ArrayList<Parametro> parametros) {
		this.nombre = nombre;
		this.numParametros = numArgumentos;
		this.codigo = codigo;
		this.isOriginal = isOriginal;
		this.parametros = parametros;
	}
	
	
	
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setNumParametros(int numParametros) {
		this.numParametros = numParametros;
	}
	public int getNumParametros() {
		return this.numParametros;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodigo() {
		return this.codigo;
	}
	public void setIsOriginal(boolean isOriginal) {
		this.isOriginal = isOriginal;
	}
	public boolean isOriginal() {
		return this.isOriginal;
	}
	public ArrayList<Parametro> getParametros() {
		return this.parametros;
	}
	public void setParametros(ArrayList<Parametro> parametros) {
		this.parametros = parametros;
	}

	/**
	 * Comprueb si el m�todo est� activado o no.
	 * @return
	 */
	public boolean isActivated() {
		if (numParametros > 0)
			return true;
		return false;
	}
	
	/**
	 * Muestra por pantalla el c�digo del m�todo.
	 */
	public void mostrarCodigo() {
		System.out.println(this.codigo);
	}
}
