package objetos;

public class Parametro {

	public static int numParametros = 0;
	
	private String nombre;
	private String tipo;
	
	public Parametro(String nombre, String tipo) {
		this.nombre = nombre;
		this.tipo = tipo;
		numParametros++;
	}
	
	public Parametro(String nombre) {
		this.nombre = nombre;
		this.tipo = "float";
		numParametros++;
	}
	
	public Parametro() {
		this.nombre = "par_" + numParametros;
		this.tipo = "float";
		numParametros++;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	public String getTipo() {
		return this.tipo;
	}
	public static void vaciarParametros() {
		numParametros = 0;
	}
	
}
