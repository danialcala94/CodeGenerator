package objetos;

public class Variable {
	
	public static int numVariables = 0;

	private String nombre;
	private String tipo;
	
	public Variable(String nombre, String tipo) {
		this.nombre = nombre;
		this.tipo = tipo;
		numVariables++;
	}
	
	public Variable(String nombre) {
		this.nombre = nombre;
		this.tipo = "float";
		numVariables++;
	}
	
	public Variable() {
		this.nombre = "aux_" + numVariables;
		this.tipo = "float";
		numVariables++;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	public String getTipo() {
		return this.tipo;
	}
	public static void vaciarVariables() {
		numVariables = 0;
	}
	
}
