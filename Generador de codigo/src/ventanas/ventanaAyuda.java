package ventanas;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import objetos.Metodo;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class ventanaAyuda extends JDialog {

	private final JPanel contentPanel = new JPanel();
	
	JButton btnResta = new JButton("Resta");
	JButton btnMultiplicacion = new JButton("Multiplicacion");
	JButton btnPotencia = new JButton("Potencia");
	JButton btnMediaAritmetica = new JButton("Media aritmetica");
	JButton btnRaizCuadrada = new JButton("Raiz cuadrada");
	JButton btnFactorial = new JButton("Factorial");
	JButton btnPorcentaje = new JButton("Porcentaje");
	JButton btnDivision = new JButton("Division");
	ArrayList<Metodo> arrayMetodos;
	ventanaCodigo ventCodigo;
	Metodo MetResta;
	Metodo MetMultiplicacion;
	Metodo MetPotencia;
	Metodo MetMultiplicion;
	Metodo MetFactorial;
	Metodo MetMedia;
	Metodo MetDivision;
	Metodo MetRaiz;
	Metodo MetPorcentaje;
	

	/**
	 * Create the dialog.
	 */
	public ventanaAyuda(ArrayList<Metodo> arrayMetodos) {
		this.setTitle("Ayuda");
		this.arrayMetodos = arrayMetodos;
		this.setModal(true);
		setBounds(100, 100, 664, 235);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblLosBotonesDe = new JLabel("Los botones de los m\u00E9todos se activar\u00E1n cuando estas operaciones se hayan implementado");
		lblLosBotonesDe.setHorizontalAlignment(SwingConstants.CENTER);
		lblLosBotonesDe.setBounds(10, 11, 631, 14);
		contentPanel.add(lblLosBotonesDe);
		{
			JLabel lblUnaVezActivado = new JLabel("Una vez activado cada bot\u00F3n mostrar\u00E1 el c\u00F3digo de su operaci\u00F3n");
			lblUnaVezActivado.setHorizontalAlignment(SwingConstants.CENTER);
			lblUnaVezActivado.setBounds(10, 36, 631, 14);
			contentPanel.add(lblUnaVezActivado);
		}
		btnResta.setEnabled(false);
		btnResta.setBounds(10, 73, 150, 23);
		contentPanel.add(btnResta);
		btnResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ventCodigo = new ventanaCodigo(MetResta);
				ventCodigo.setVisible(true);
				ventCodigo.setModal(true);
			}
		});

		btnMultiplicacion.setEnabled(false);
		btnMultiplicacion.setBounds(170, 73, 150, 23);
		contentPanel.add(btnMultiplicacion);
		btnMultiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ventCodigo = new ventanaCodigo(MetMultiplicacion);
				ventCodigo.setVisible(true);
				ventCodigo.setModal(true);
			}
		});

		btnPotencia.setEnabled(false);
		btnPotencia.setBounds(330, 73, 150, 23);
		contentPanel.add(btnPotencia);
		btnPotencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ventCodigo = new ventanaCodigo(MetPotencia);
				ventCodigo.setVisible(true);
				ventCodigo.setModal(true);
			}
		});

		btnMediaAritmetica.setEnabled(false);
		btnMediaAritmetica.setBounds(491, 73, 150, 23);
		contentPanel.add(btnMediaAritmetica);
		btnMediaAritmetica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ventCodigo = new ventanaCodigo(MetMedia);
				ventCodigo.setVisible(true);
				ventCodigo.setModal(true);
			}
		});


		btnRaizCuadrada.setEnabled(false);
		btnRaizCuadrada.setBounds(10, 121, 150, 23);
		contentPanel.add(btnRaizCuadrada);
		btnRaizCuadrada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventCodigo = new ventanaCodigo(MetRaiz);
				ventCodigo.setVisible(true);
				ventCodigo.setModal(true);
			}
		});

		btnFactorial.setEnabled(false);
		btnFactorial.setBounds(170, 121, 150, 23);
		contentPanel.add(btnFactorial);
		btnFactorial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventCodigo = new ventanaCodigo(MetFactorial);
				ventCodigo.setVisible(true);
				ventCodigo.setModal(true);
			}
		});

		btnPorcentaje.setEnabled(false);
		btnPorcentaje.setBounds(330, 121, 150, 23);
		contentPanel.add(btnPorcentaje);
		btnPorcentaje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventCodigo = new ventanaCodigo(MetPorcentaje);
				ventCodigo.setVisible(true);
				ventCodigo.setModal(true);
			}
		});

		btnDivision.setEnabled(false);
		btnDivision.setBounds(491, 121, 150, 23);
		contentPanel.add(btnDivision);
		btnDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ventCodigo = new ventanaCodigo(MetDivision);
				ventCodigo.setVisible(true);
				ventCodigo.setModal(true);
			}
		});
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 155, 648, 33);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
			}
		}
		
		activaBotones();
	}
	
	private void activaBotones() {
		for (int i = 0; i < arrayMetodos.size(); i++) {
			if (arrayMetodos.get(i).isActivated()) {
				switch (arrayMetodos.get(i).getNombre()) {
				case "Resta":
					btnResta.setEnabled(true);
					MetResta = arrayMetodos.get(i);
					break;
				case "Potencia":
					btnPotencia.setEnabled(true);
					MetPotencia = arrayMetodos.get(i);
					break;
				case "Factorial":
					btnFactorial.setEnabled(true);
					MetFactorial = arrayMetodos.get(i);
					break;
				case "Media aritmética":
					btnMediaAritmetica.setEnabled(true);
					MetMedia = arrayMetodos.get(i);
					break;
				case "Raiz cuadrada":
					btnRaizCuadrada.setEnabled(true);
					MetRaiz = arrayMetodos.get(i);
					break;
				case "Multiplicación":
					btnMultiplicacion.setEnabled(true);
					MetMultiplicacion = arrayMetodos.get(i);
					break;
				case "Porcentaje":
					btnPorcentaje.setEnabled(true);
					MetPorcentaje = arrayMetodos.get(i);
					break;
				case "División":
					btnDivision.setEnabled(true);
					MetDivision = arrayMetodos.get(i);
					break;
				}
			}
		}
	}
}
