package ventanas;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.layout.FormLayout.Measure;

import javafx.scene.input.KeyCode;
import objetos.Metodo;
import objetos.Parametro;
import objetos.Variable;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

public class VentanaMultiplicacion extends JDialog {
	
	final String nombreMetodo = "Multiplicaci�n";
	String nombreFuncion ="multiplicacion";
	public String resultado = null;
	
	private JTextField textField_0 = new JTextField();
	private JTextField textField_1 = new JTextField();
	private JTextField textField_2 = new JTextField();
	private JTextField textField_3 = new JTextField();
	private JTextField textField_4 = new JTextField();
	private JTextField textField_5 = new JTextField();
	private JTextField textField_6 = new JTextField();
	private JTextField textField_7 = new JTextField();
	private JTextField textField_8 = new JTextField();
	private JTextField textField_9 = new JTextField();
	ArrayList<JTextField> textFields = new ArrayList<JTextField>();
	JRadioButton rad0B = new JRadioButton("");
	JRadioButton rad0A = new JRadioButton("");
	JRadioButton rad1B = new JRadioButton("");
	JRadioButton rad1A = new JRadioButton("");
	JRadioButton rad2B = new JRadioButton("");
	JRadioButton rad2A = new JRadioButton("");
	JRadioButton rad3B = new JRadioButton("");
	JRadioButton rad3A = new JRadioButton("");
	JRadioButton rad4B = new JRadioButton("");
	JRadioButton rad4A = new JRadioButton("");
	JRadioButton rad5B = new JRadioButton("");
	JRadioButton rad5A = new JRadioButton("");
	JRadioButton rad6B = new JRadioButton("");
	JRadioButton rad6A = new JRadioButton("");
	JRadioButton rad7B = new JRadioButton("");
	JRadioButton rad7A = new JRadioButton("");
	JRadioButton rad8B = new JRadioButton("");
	JRadioButton rad8A = new JRadioButton("");
	JRadioButton rad9B = new JRadioButton("");
	JRadioButton rad9A = new JRadioButton("");
	ArrayList<JRadioButton> radioButtons = new ArrayList<JRadioButton>();
	private ButtonGroup bt0 = new ButtonGroup();
	private ButtonGroup bt1 = new ButtonGroup();
	private ButtonGroup bt2 = new ButtonGroup();
	private ButtonGroup bt3 = new ButtonGroup();
	private ButtonGroup bt4 = new ButtonGroup();
	private ButtonGroup bt5 = new ButtonGroup();
	private ButtonGroup bt6 = new ButtonGroup();
	private ButtonGroup bt7 = new ButtonGroup();
	private ButtonGroup bt8 = new ButtonGroup();
	private ButtonGroup bt9 = new ButtonGroup();
	JComboBox comboBox_0 = new JComboBox();
	JComboBox comboBox_1 = new JComboBox();
	JComboBox comboBox_2 = new JComboBox();
	JComboBox comboBox_3 = new JComboBox();
	JComboBox comboBox_4 = new JComboBox();
	JComboBox comboBox_5 = new JComboBox();
	JComboBox comboBox_6 = new JComboBox();
	JComboBox comboBox_7 = new JComboBox();
	JComboBox comboBox_8 = new JComboBox();
	JComboBox comboBox_9 = new JComboBox();
	JComboBox comboBox_GuardarResultado = new JComboBox();
	ArrayList<JComboBox> comboBoxes = new ArrayList<JComboBox>();
	JCheckBox chckbxGuardarResultado = new JCheckBox("Guardar resultado en");
	
	JButton okButton = new JButton("OK");
	boolean okActivado = false;
	JButton btnMostrar = new JButton("Mostrar c\u00F3digo");
	
	private ArrayList<Metodo> arrayMetodos;
	private ArrayList<Parametro> arrayParametros;
	private ArrayList<Variable> arrayVariables;
	private Metodo esteMetodo;
	private int numParam;
	
	public ArrayList<JComboBox> getComboBoxes() {
		return this.comboBoxes;
	}
	public ArrayList<JTextField> getTextFields() {
		return this.textFields;
	}
	
	
	private void gestionaMetodo(){
		String result = nombreFuncion+"(";
		boolean gestionar = true;
		// Comprobar si se han introducido valores
		//System.out.println("Se ha pulsado OK");
		for (int i = 0; i < esteMetodo.getNumParametros() * 2; i += 2) {
			textFields.get(i).setText(textFields.get(i).getText().trim());
			if (radioButtons.get(i+1).isSelected()) {
				// Comprobar textField
				JTextField textField = textFields.get(i / 2);
				if (textField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null,
		                    "Debe introducir un valor para el par�metro " + i / 2 + ".",
		                    "nombreMetodo",
		                    JOptionPane.ERROR_MESSAGE);
					gestionar = false;
					return;
				}
			}
		}
		
		if (gestionar) {
			for (int i = 0; i < esteMetodo.getNumParametros() * 2; i += 2) {
				// radiA -> �Funci�n?
				if (radioButtons.get(i).isSelected()) {
					//System.out.println("Ejecutar lo correspondiente a [ComboBox] rad" + i / 2 + "A => " + comboBoxes.get(i / 2).getSelectedItem());
					if(comboBoxes.get(i / 2).getSelectedItem().toString().contains("par_") || comboBoxes.get(i/2).getSelectedItem().toString().contains("aux_")){
						result += comboBoxes.get(i / 2).getSelectedItem().toString()+", ";
					}else{
						result += llamaFuncion(comboBoxes.get(i / 2).getSelectedItem().toString())+", ";
					}
					// Con esto
					//Esto no se puede hacer todavia si los metodos lo estan bien implementados
					//obtenerMetodo(comboBoxes.get(i/ 2 ).getSelectedItem().toString()).getCodigo();
				} else {
					//System.out.println("Ejecutar lo correspondiente a [Texto] rad" + i / 2 + "B => " + textFields.get(i / 2).getText());
					result += textFields.get(i / 2).getText()+", ";
				// radiB -> �Valor introducido por teclado?
				}
			}
		}
		//Quito el ultimo espacio a�adido
		result = result.substring(0, result.length()-2);
		result += ")";
		if(chckbxGuardarResultado.isSelected()) {
			result = comboBox_GuardarResultado.getSelectedItem().toString()+" = " +result;
		}
		if(result.contains("null")) {
			this.resultado = null;
		}else{
			this.resultado = result;
		}
		dispose();
	}
	
	private String llamaFuncion(String operacion) {
		String result = null;
		switch (operacion) {
		case "Suma":
			VentanaSuma suma = new VentanaSuma(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			suma.setVisible(true);
			result = suma.resultado;
			return result;
			 
		case "Negaci�n":
			VentanaNegacion neg = new VentanaNegacion(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			neg.setVisible(true);
			result = neg.resultado;
			return result;
			
		case "Resta":
			VentanaResta resta = new VentanaResta(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			resta.setVisible(true);
			result = resta.resultado;
			return result;
			
		case "Potencia":
			VentanaPotencia potencia = new VentanaPotencia(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			potencia.setVisible(true);
			result = potencia.resultado;
			
			return result;
			
		case "Factorial":
			VentanaFactorial factorial = new VentanaFactorial(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			factorial.setVisible(true);
			result = factorial.resultado;
			
			return result;
			
		case "Media artim�tica":
			VentanaMediaAritmetica mediaAritmetica = new VentanaMediaAritmetica(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			mediaAritmetica.setVisible(true);
			result = mediaAritmetica.resultado;
			
			return result;
			
		case "Raiz cuadrada":
			VentanaRaizCuadrada raizCuadrada = new VentanaRaizCuadrada(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			raizCuadrada.setVisible(true);
			result = raizCuadrada.resultado;
			
			return result;
			
		case "Multiplicaci�n":
			VentanaMultiplicacion multiplicacion = new VentanaMultiplicacion(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			multiplicacion.setVisible(true);
			result = multiplicacion.resultado;
			
			return result;
			
		case "Porcentaje":
			VentanaPorcentaje porcentaje = new VentanaPorcentaje(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			porcentaje.setVisible(true);
			result = porcentaje.resultado;
			
			return result;
			
		case "Divisi�n":
			VentanaDivision division = new VentanaDivision(this.numParam, arrayMetodos, arrayParametros, arrayVariables);
			division.setVisible(true);
			result = division.resultado;
			
			return result;
		

		default:
			//System.out.println("Sin implementar todavia. NO DEBERIA IMPRIMIR ESTO");

			break;
		}
		return null;
	}
	
	/**
	 * Create the dialog.
	 */
	public VentanaMultiplicacion(int numParm, ArrayList<Metodo> arrayMetodos, ArrayList<Parametro> arrayParametros, ArrayList<Variable> arrayVariables) {
		this.numParam = numParm;
		setResizable(false);
		this.setModal(true);
		this.arrayMetodos = arrayMetodos;
		this.arrayParametros = arrayParametros;
		this.arrayVariables = arrayVariables;
		asignaRadioButtons();
		identificaMetodo();
		setTitle(esteMetodo.getNombre());


		setBounds(100, 100, 914, 391);
		getContentPane().setLayout(null);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(10, 331, 902, 33);
			getContentPane().add(buttonPane);
			buttonPane.setLayout(null);
			{
				okButton = new JButton("OK");
				okButton.setEnabled(false);
				okButton.setBounds(741, 0, 58, 23);
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						// Comprobar aqu� si los valores introducidos son correctos
						gestionaMetodo();
					}
					
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setBounds(804, 0, 86, 23);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				
				JPanel panel = new JPanel();
				panel.setBounds(10, 11, 902, 307);
				getContentPane().add(panel);
				panel.setLayout(null);
				
				
				// Par�metro 0
				JPanel panel_1 = new JPanel();
				panel_1.setLayout(null);
				panel_1.setBounds(12, 13, 130, 115);
				panel.add(panel_1);
				
				textField_0 = new JTextField();
				textField_0.setEnabled(false);
				textField_0.setColumns(10);
				textField_0.setBounds(39, 80, 76, 22);
				panel_1.add(textField_0);
				rad0B.setEnabled(false);
				rad0B.setBounds(0, 77, 30, 25);
				panel_1.add(rad0B);
				
				rad0A.setEnabled(false);
				rad0A.setBounds(0, 36, 30, 25);
				panel_1.add(rad0A);
				
				comboBox_0 = new JComboBox();
				comboBox_0.setEnabled(false);
				comboBox_0.setBounds(39, 38, 76, 20);
				panel_1.add(comboBox_0);
				
				JLabel label = new JLabel("Parametro_0");
				label.setHorizontalAlignment(SwingConstants.CENTER);
				label.setBounds(39, 13, 76, 14);
				panel_1.add(label);
				// Par�metro 0
				
				
				// Par�metro 1
				JPanel panel_2 = new JPanel();
				panel_2.setLayout(null);
				panel_2.setBounds(154, 13, 130, 115);
				panel.add(panel_2);
				
				textField_1 = new JTextField();
				textField_1.setEnabled(false);
				textField_1.setColumns(10);
				textField_1.setBounds(39, 80, 76, 22);
				panel_2.add(textField_1);
				rad1B.setEnabled(false);
				
				rad1B.setBounds(0, 77, 30, 25);
				panel_2.add(rad1B);
				rad1A.setEnabled(false);
				
				rad1A.setBounds(0, 36, 30, 25);
				panel_2.add(rad1A);
				
				comboBox_1 = new JComboBox();
				comboBox_1.setEnabled(false);
				comboBox_1.setBounds(39, 38, 76, 20);
				panel_2.add(comboBox_1);
				
				JLabel label_1 = new JLabel("Parametro_1");
				label_1.setHorizontalAlignment(SwingConstants.CENTER);
				label_1.setBounds(39, 13, 76, 14);
				panel_2.add(label_1);
				// Par�metro 1
				
				
				// Par�metro 2
				JPanel panel_3 = new JPanel();
				panel_3.setLayout(null);
				panel_3.setBounds(296, 13, 130, 115);
				panel.add(panel_3);
				
				textField_2 = new JTextField();
				textField_2.setEnabled(false);
				textField_2.setColumns(10);
				textField_2.setBounds(39, 80, 76, 22);
				panel_3.add(textField_2);
				rad2B.setEnabled(false);
				
				rad2B.setBounds(0, 77, 30, 25);
				panel_3.add(rad2B);
				rad2A.setEnabled(false);
				
				rad2A.setBounds(0, 36, 30, 25);
				panel_3.add(rad2A);
				
				comboBox_2 = new JComboBox();
				comboBox_2.setEnabled(false);
				comboBox_2.setBounds(39, 38, 76, 20);
				panel_3.add(comboBox_2);
				
				JLabel lblParametro = new JLabel("Parametro_2");
				lblParametro.setHorizontalAlignment(SwingConstants.CENTER);
				lblParametro.setBounds(39, 13, 76, 14);
				panel_3.add(lblParametro);
				// Par�metro 2
				
				
				// Par�metro 3
				JPanel panel_4 = new JPanel();
				panel_4.setLayout(null);
				panel_4.setBounds(438, 13, 130, 115);
				panel.add(panel_4);
				
				textField_3 = new JTextField();
				textField_3.setEnabled(false);
				textField_3.setColumns(10);
				textField_3.setBounds(39, 80, 76, 22);
				panel_4.add(textField_3);
				rad3B.setEnabled(false);
				
				rad3B.setBounds(0, 77, 30, 25);
				panel_4.add(rad3B);
				rad3A.setEnabled(false);
				
				rad3A.setBounds(0, 36, 30, 25);
				panel_4.add(rad3A);
				
				comboBox_3 = new JComboBox();
				comboBox_3.setEnabled(false);
				comboBox_3.setBounds(39, 38, 76, 20);
				panel_4.add(comboBox_3);
				
				JLabel lblParametro_1 = new JLabel("Parametro_3");
				lblParametro_1.setHorizontalAlignment(SwingConstants.CENTER);
				lblParametro_1.setBounds(39, 13, 76, 14);
				panel_4.add(lblParametro_1);
				// Par�metro 3
				
				
				// Par�metro 4
				JPanel panel_5 = new JPanel();
				panel_5.setLayout(null);
				panel_5.setBounds(580, 13, 130, 115);
				panel.add(panel_5);
				
				textField_4 = new JTextField();
				textField_4.setEnabled(false);
				textField_4.setColumns(10);
				textField_4.setBounds(39, 80, 76, 22);
				panel_5.add(textField_4);
				rad4B.setEnabled(false);
				
				rad4B.setBounds(0, 77, 30, 25);
				panel_5.add(rad4B);
				rad4A.setEnabled(false);
				
				rad4A.setBounds(0, 36, 30, 25);
				panel_5.add(rad4A);
				
				comboBox_4 = new JComboBox();
				comboBox_4.setEnabled(false);
				comboBox_4.setBounds(39, 38, 76, 20);
				panel_5.add(comboBox_4);
				
				JLabel lblParametro_2 = new JLabel("Parametro_4");
				lblParametro_2.setHorizontalAlignment(SwingConstants.CENTER);
				lblParametro_2.setBounds(39, 13, 76, 14);
				panel_5.add(lblParametro_2);
				// Par�metro 4
				
				
				// Par�metro 5
				JPanel panel_6 = new JPanel();
				panel_6.setLayout(null);
				panel_6.setBounds(12, 163, 130, 115);
				panel.add(panel_6);
				
				textField_5 = new JTextField();
				textField_5.setEnabled(false);
				textField_5.setColumns(10);
				textField_5.setBounds(39, 80, 76, 22);
				panel_6.add(textField_5);
				rad5B.setEnabled(false);
				
				rad5B.setBounds(0, 77, 30, 25);
				panel_6.add(rad5B);
				rad5A.setEnabled(false);
				
				rad5A.setBounds(0, 36, 30, 25);
				panel_6.add(rad5A);
				
				comboBox_5 = new JComboBox();
				comboBox_5.setEnabled(false);
				comboBox_5.setBounds(39, 38, 76, 20);
				panel_6.add(comboBox_5);
				
				JLabel lblParametro_3 = new JLabel("Parametro_5");
				lblParametro_3.setHorizontalAlignment(SwingConstants.CENTER);
				lblParametro_3.setBounds(39, 13, 76, 14);
				panel_6.add(lblParametro_3);
				// Par�metro 5
				
				
				// Par�metro 6
				JPanel panel_7 = new JPanel();
				panel_7.setLayout(null);
				panel_7.setBounds(154, 163, 130, 115);
				panel.add(panel_7);
				
				textField_6 = new JTextField();
				textField_6.setEnabled(false);
				textField_6.setColumns(10);
				textField_6.setBounds(39, 80, 76, 22);
				panel_7.add(textField_6);
				rad6B.setEnabled(false);
				
				rad6B.setBounds(0, 77, 30, 25);
				panel_7.add(rad6B);
				rad6A.setEnabled(false);
				
				rad6A.setBounds(0, 36, 30, 25);
				panel_7.add(rad6A);
				
				comboBox_6 = new JComboBox();
				comboBox_6.setEnabled(false);
				comboBox_6.setBounds(39, 38, 76, 20);
				panel_7.add(comboBox_6);
				
				JLabel lblParametro_4 = new JLabel("Parametro_6");
				lblParametro_4.setHorizontalAlignment(SwingConstants.CENTER);
				lblParametro_4.setBounds(39, 13, 76, 14);
				panel_7.add(lblParametro_4);
				// Par�metro 6
				
				
				// Par�metro 7
				JPanel panel_8 = new JPanel();
				panel_8.setLayout(null);
				panel_8.setBounds(296, 163, 130, 115);
				panel.add(panel_8);
				
				textField_7 = new JTextField();
				textField_7.setEnabled(false);
				textField_7.setColumns(10);
				textField_7.setBounds(39, 80, 76, 22);
				panel_8.add(textField_7);
				rad7B.setEnabled(false);
				
				rad7B.setBounds(0, 77, 30, 25);
				panel_8.add(rad7B);
				rad7A.setEnabled(false);
				
				rad7A.setBounds(0, 36, 30, 25);
				panel_8.add(rad7A);
				
				comboBox_7 = new JComboBox();
				comboBox_7.setEnabled(false);
				comboBox_7.setBounds(39, 38, 76, 20);
				panel_8.add(comboBox_7);
				
				JLabel lblParametro_5 = new JLabel("Parametro_7");
				lblParametro_5.setHorizontalAlignment(SwingConstants.CENTER);
				lblParametro_5.setBounds(39, 13, 76, 14);
				panel_8.add(lblParametro_5);
				// Par�metro 7
				
				
				// Par�metro 8
				JPanel panel_9 = new JPanel();
				panel_9.setLayout(null);
				panel_9.setBounds(438, 163, 130, 115);
				panel.add(panel_9);
				
				textField_8 = new JTextField();
				textField_8.setEnabled(false);
				textField_8.setColumns(10);
				textField_8.setBounds(39, 80, 76, 22);
				panel_9.add(textField_8);
				rad8B.setEnabled(false);
				
				rad8B.setBounds(0, 77, 30, 25);
				panel_9.add(rad8B);
				rad8A.setEnabled(false);
				
				rad8A.setBounds(0, 36, 30, 25);
				panel_9.add(rad8A);
				
				comboBox_8 = new JComboBox();
				comboBox_8.setEnabled(false);
				comboBox_8.setBounds(39, 38, 76, 20);
				panel_9.add(comboBox_8);
				
				
				JLabel lblParametro_6 = new JLabel("Parametro_8");
				lblParametro_6.setHorizontalAlignment(SwingConstants.CENTER);
				lblParametro_6.setBounds(39, 13, 76, 14);
				panel_9.add(lblParametro_6);
				// Par�metro 8
				
				
				// Par�metro 9
				JPanel panel_10 = new JPanel();
				panel_10.setLayout(null);
				panel_10.setBounds(580, 163, 130, 115);
				panel.add(panel_10);
				
				textField_9 = new JTextField();
				textField_9.setEnabled(false);
				textField_9.setColumns(10);
				textField_9.setBounds(39, 80, 76, 22);
				panel_10.add(textField_9);
				rad9B.setEnabled(false);
				
				rad9B.setBounds(0, 77, 30, 25);
				panel_10.add(rad9B);
				rad9A.setEnabled(false);
				
				rad9A.setBounds(0, 36, 30, 25);
				panel_10.add(rad9A);
				
				comboBox_9 = new JComboBox();
				comboBox_9.setEnabled(false);
				comboBox_9.setBounds(39, 38, 76, 20);
				panel_10.add(comboBox_9);
				
				JLabel lblParametro_7 = new JLabel("Parametro_9");
				lblParametro_7.setHorizontalAlignment(SwingConstants.CENTER);
				lblParametro_7.setBounds(39, 13, 76, 14);
				panel_10.add(lblParametro_7);
				// Par�metro 9
				
				
				
				// Guardar resultado en
				JPanel panel_GuardarResultado = new JPanel();
				panel_GuardarResultado.setBounds(720, 49, 151, 80);
				panel.add(panel_GuardarResultado);
				panel_GuardarResultado.setLayout(null);
				
				comboBox_GuardarResultado = new JComboBox();
				comboBox_GuardarResultado.setEnabled(false);
				comboBox_GuardarResultado.setBounds(38, 43, 76, 20);
				
				chckbxGuardarResultado.setBounds(8, 9, 131, 25);
				chckbxGuardarResultado.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						if (chckbxGuardarResultado.isSelected())
							comboBox_GuardarResultado.setEnabled(true);
						else
							comboBox_GuardarResultado.setEnabled(false);
					}
				});
				// Guardar resultado en
				panel_GuardarResultado.add(chckbxGuardarResultado);
				panel_GuardarResultado.add(comboBox_GuardarResultado);
				
				
				btnMostrar.setBounds(736, 189, 122, 23);
				panel.add(btnMostrar);
				btnMostrar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ventanaCodigo ventCod = new ventanaCodigo(esteMetodo);
						ventCod.setVisible(true);
						ventCod.setModal(true);
					}
				});

				
				
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();				
					}
				});
				

				habilitaBotones();
				// Almacena los elementos necesitados en arrays para agilizar su tratamiento
				almacenaEnArrays();
				addActionListeners();
			}
		}
	}
	
	private void asignaRadioButtons(){
		bt0.add(rad0B);
		bt0.add(rad0A);
		bt1.add(rad1B);
		bt1.add(rad1A);
		bt2.add(rad2B);
		bt2.add(rad2A);
		bt3.add(rad3B);
		bt3.add(rad3A);
		bt4.add(rad4B);
		bt4.add(rad4A);
		bt5.add(rad5B);
		bt5.add(rad5A);
		bt6.add(rad6B);
		bt6.add(rad6A);
		bt7.add(rad7B);
		bt7.add(rad7A);
		bt8.add(rad8B);
		bt8.add(rad8A);
		bt9.add(rad9B);
		bt9.add(rad9A);
	}
	
	private void habilitaBotones() {
		switch (esteMetodo.getNumParametros()) {
		default:
			break;
		case 10:
			rad9A.setEnabled(true);
			rad9B.setEnabled(true);
			
		case 9:
			rad8A.setEnabled(true);
			rad8B.setEnabled(true);
			
		case 8:
			rad7A.setEnabled(true);
			rad7B.setEnabled(true);
			
		case 7:
			rad6A.setEnabled(true);
			rad6B.setEnabled(true);
			
		case 6:
			rad5A.setEnabled(true);
			rad5B.setEnabled(true);
			
		case 5:
			rad4A.setEnabled(true);
			rad4B.setEnabled(true);
			
		case 4:
			rad3A.setEnabled(true);
			rad3B.setEnabled(true);
			
		case 3:
			rad2A.setEnabled(true);
			rad2B.setEnabled(true);
			
		case 2:
			rad1A.setEnabled(true);
			rad1B.setEnabled(true);
			
		case 1:
			rad0A.setEnabled(true);
			rad0B.setEnabled(true);
		}
	}
	
	private void identificaMetodo() {
		esteMetodo = obtenerMetodo(nombreMetodo);
	}
	
	private Metodo obtenerMetodo(String nombre) {
		Metodo metodo = null;
		for (int i = 0; i < arrayMetodos.size(); i++) {
			if (arrayMetodos.get(i).getNombre().equals(nombre)) {
				metodo = arrayMetodos.get(i);
				break;
			}
		}
		return metodo;
	}
	
	/**
	 * Facilita la lectura, el cambio, y todas las funciones sobre los JTextFields, JComboBoxes y RadioButtons.
	 * Con un bucle, dependiendo del n�mero de par�metros introducidos, simplemente leeremos de los elementos por posiciones, evitando escribir c�digo de m�s.
	 */
	private void almacenaEnArrays() {
		//System.out.println("almacenaEnArrays()");
		// JTextFields
		textFields.add(textField_0);
		textFields.add(textField_1);
		textFields.add(textField_2);
		textFields.add(textField_3);
		textFields.add(textField_4);
		textFields.add(textField_5);
		textFields.add(textField_6);
		textFields.add(textField_7);
		textFields.add(textField_8);
		textFields.add(textField_9);
		
		// JComboBoxes
		comboBoxes.add(comboBox_0);
		comboBoxes.add(comboBox_1);
		comboBoxes.add(comboBox_2);
		comboBoxes.add(comboBox_3);
		comboBoxes.add(comboBox_4);
		comboBoxes.add(comboBox_5);
		comboBoxes.add(comboBox_6);
		comboBoxes.add(comboBox_7);
		comboBoxes.add(comboBox_8);
		comboBoxes.add(comboBox_9);
		ArrayList<Metodo> metodosActivados = obtenerMetodosActivados();
		// Para cada uno de los comboBoxes activados (solo los seleccionables)
		for (int i = 0; i < esteMetodo.getNumParametros(); i++) {
			for (int j = 0; j < metodosActivados.size(); j++) {
				if(!metodosActivados.get(j).getNombre().equalsIgnoreCase("Repetici�n")){
					comboBoxes.get(i).addItem(metodosActivados.get(j).getNombre());
				}
			}
			for (int j = 0; j < arrayParametros.size(); j++) {
				comboBoxes.get(i).addItem(arrayParametros.get(j).getNombre());
			}
			for(int j=0; j< arrayVariables.size(); j++) {
				comboBoxes.get(i).addItem(arrayVariables.get(j).getNombre());
			}
		}
		
		// RadioButtons
		radioButtons.add(rad0A);
		radioButtons.add(rad0B);
		radioButtons.add(rad1A);
		radioButtons.add(rad1B);
		radioButtons.add(rad2A);
		radioButtons.add(rad2B);
		radioButtons.add(rad3A);
		radioButtons.add(rad3B);
		radioButtons.add(rad4A);
		radioButtons.add(rad4B);
		radioButtons.add(rad5A);
		radioButtons.add(rad5B);
		radioButtons.add(rad6A);
		radioButtons.add(rad6B);
		radioButtons.add(rad7A);
		radioButtons.add(rad7B);
		radioButtons.add(rad8A);
		radioButtons.add(rad8B);
		radioButtons.add(rad9A);
		radioButtons.add(rad9B);
		for (int i = 0; i < radioButtons.size(); i++) {
			radioButtons.get(i).addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					comprobarSiHabilitarOk();
				}
			});
		}
		
		for (int i = 0; i < arrayParametros.size(); i++) {
			comboBox_GuardarResultado.addItem(arrayParametros.get(i).getNombre());
		}
		for(int i=0; i< arrayVariables.size(); i++) {
			comboBox_GuardarResultado.addItem(arrayVariables.get(i).getNombre());
		}
	}
	
	private void comprobarSiHabilitarOk() {
		if (!okActivado) {
			//Se multiplica por 2 porque cada grupo de botones pose dos radio buttons
			int radioButtonsActivados = esteMetodo.getNumParametros() * 2;
			int radioButtonsElegidos = 0;
			for (int i = 0; i < radioButtonsActivados; i+=2) {
				if (radioButtons.get(i).isSelected() || radioButtons.get(i+1).isSelected())
					radioButtonsElegidos++;
			}
			if (radioButtonsElegidos == radioButtonsActivados / 2) {
				okButton.setEnabled(true);
				// Evita que, una vez activado el OK, se vuelvan a gastar recursos con esta funci�n
				okActivado = true;
			}
		}
	}
	
	private ArrayList<Metodo> obtenerMetodosActivados() {
		ArrayList<Metodo> metodosActivados = new ArrayList<Metodo>();
		for (int i = 0; i < arrayMetodos.size(); i++) {
			if (arrayMetodos.get(i).isActivated())
				metodosActivados.add(arrayMetodos.get(i));
		}
		return metodosActivados;
	}
	
	
	private void generaCampos(int numParametros) {
		for(int i=0; i<numParametros; i++) {
			//System.out.println("Creamos el campo "+i);
			
		}
	}
	
	private void addActionListeners(){
		// si radiA activado 	=> comboBox activado
		//						=> textField desactivado
		// si radiB activado 	=> comboBox desactivado
		//						=> textField activado
		for (int i = 0; i < radioButtons.size(); i += 2) {
			// radiA
			final int j = i;
			radioButtons.get(i).addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					getComboBoxes().get(j / 2).setEnabled(true);
					getTextFields().get(j / 2).setEnabled(false);
				}
			});
			// radiB
			radioButtons.get(i+1).addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					getComboBoxes().get(j / 2).setEnabled(false);
					getTextFields().get(j / 2).setEnabled(true);
				}
			});
			textFields.get(i / 2).addKeyListener(new KeyAdapter() {
				public void keyTyped(KeyEvent e)
				   {
				      char caracter = e.getKeyChar();
				      // Verificar si la tecla pulsada no es un digito
				      if(((caracter < '0') ||
				         (caracter > '9')) && (caracter != '-') &&
				         (caracter != '\b' /*corresponde a BACK_SPACE*/) && (caracter != '.'))
				      {
				    	  JOptionPane.showMessageDialog(null,
				                    "Solo se admiten cifras num�ricas, puntos o signos negativos.",
				                    "Par�metro " + j / 2,
				                    JOptionPane.ERROR_MESSAGE);
				    	  e.consume();  // ignorar el evento de teclado
				      }
				   }
					
			});
			textFields.get(i / 2).addFocusListener(new FocusListener() {

				@Override
				public void focusGained(FocusEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void focusLost(FocusEvent arg0) {
					// TODO Auto-generated method stub
					boolean fallo = false;
					if (textFields.get(j / 2).getText().length() > 9) {
						JOptionPane.showMessageDialog(null,
			                    "El m�ximo es 9 d�gitos.",
			                    "Par�metro " + j / 2,
			                    JOptionPane.ERROR_MESSAGE);
						textFields.get(j / 2).setText(textFields.get(j / 2).getText().substring(0, 9));
						textFields.get(j / 2).grabFocus();
						fallo = true;
					}
					
					
					int numGuion = 0, numPunto = 0;		String cad = textFields.get(j / 2).getText();
					if (!fallo) {
						for (int i = 0; i < cad.length(); i++) {
							if (cad.charAt(i) == '.')
								numPunto++;
							if (cad.charAt(i) == '-')
								numGuion++;
							if (numPunto > 1 || numGuion > 1) {
								JOptionPane.showMessageDialog(null,
					                    "Ha introducido m�s de un punto o m�s de un signo negativo.\nUn n�mero decimal negativo debe ir con el formato -3.75",
					                    "nombreMetodo",
					                    JOptionPane.ERROR_MESSAGE);
								textFields.get(j / 2).setText("");
								textFields.get(j / 2).grabFocus();
								fallo = true;
								break;
							}
						}
					}
					
					if (textFields.get(j / 2).getText().contains(".") && textFields.get(j / 2).getText().contains("-") && !fallo) {
						
						if (textFields.get(j / 2).getText().charAt(0) != '-' || textFields.get(j / 2).getText().charAt(0) == '.' || textFields.get(j / 2).getText().charAt(textFields.get(j / 2).getText().length() - 1) == '.') {
							JOptionPane.showMessageDialog(null,
				                    "Para introducir un valor decimal negativo escriba un signo menos (-), la parte entera, un punto, y la parte decimal. Por ejemplo: -0.75",
				                    "nombreMetodo",
				                    JOptionPane.ERROR_MESSAGE);
							textFields.get(j / 2).setText("");
							textFields.get(j / 2).grabFocus();
						}
					} else if (textFields.get(j / 2).getText().contains(".") && !fallo) {
						if (textFields.get(j / 2).getText().charAt(0) == '.' || textFields.get(j / 2).getText().charAt(textFields.get(j / 2).getText().length() - 1) == '.') {
							JOptionPane.showMessageDialog(null,
				                    "Para introducir un valor decimal escriba la parte entera, un punto, y la parte decimal. Por ejemplo: 0.75",
				                    "Par�metro " + j / 2,
				                    JOptionPane.ERROR_MESSAGE);
							textFields.get(j / 2).setText("");
							textFields.get(j / 2).grabFocus();
						}
					} else if (textFields.get(j / 2).getText().contains("-") && !fallo) {
						if (textFields.get(j / 2).getText().charAt(0) != '-') {
							JOptionPane.showMessageDialog(null,
									"Para negar el valor, introduzca '-' seguido del n�mero. Por ejemplo: -345",
				                    "Par�metro " + j / 2,
				                    JOptionPane.ERROR_MESSAGE);
							textFields.get(j / 2).setText("");
							textFields.get(j / 2).grabFocus();
						}
					}
				}
			});
		}		
	}
	
	
	private boolean esNumero(String texto){
		try{
			Integer.parseInt(texto);
			return true;
		}catch (Exception e) {
			return false;
		}
	}
}