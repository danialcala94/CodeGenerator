package ventanas;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import objetos.Metodo;
import objetos.Parametro;
import objetos.Variable;
import javax.swing.JCheckBox;

public class VentanaNegacion extends JDialog {

	private final JPanel contentPanel = new JPanel();
	JComboBox cmbOperacion = new JComboBox();
	JCheckBox chckbxGuardarResultado;
	JComboBox comboBox_GuardaResultado;
	public String resultado;
	ArrayList<Metodo> metodos;
	ArrayList<Parametro> parametros;
	ArrayList<Variable> variables;
	int numParm;

	/**
	 * Create the dialog.
	 */
	public VentanaNegacion(int numParm, ArrayList<Metodo> metodos, ArrayList<Parametro> parametros, ArrayList<Variable> variables) {
		this.metodos = metodos;
		this.parametros = parametros;
		this.variables = variables;
		this.numParm = numParm;
		JButton okButton = new JButton("OK");
		setModal(true);
		setBounds(100, 100, 244, 267);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		this.setTitle("Negaci�n");
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		setResizable(false);
		contentPanel.setLayout(null);
		
		JLabel lblIntro = new JLabel("Par\u00E1metro a negar:");
		lblIntro.setBounds(12, 13, 165, 16);
		contentPanel.add(lblIntro);
		
		
		cmbOperacion.setBounds(12, 42, 165, 22);
		
		//Aqui a�ade todas las operaciones
		//Excepto la repeticion, que no tendria sentido
		for (int i = 0; i < metodos.size(); i++) {
			if (metodos.get(i).isActivated()){
				if(!metodos.get(i).getNombre().equals("Repetici�n")){
					//La operacion repeticion no se a�ade a la lista de opciones
					cmbOperacion.addItem(metodos.get(i).getNombre());
				}
			}
		}
		comboBox_GuardaResultado = new JComboBox();
		comboBox_GuardaResultado.setBounds(12, 128, 165, 22);
		comboBox_GuardaResultado.setEnabled(false);
		contentPanel.add(comboBox_GuardaResultado);
		for (int i = 0; i < parametros.size(); i++) {
			cmbOperacion.addItem(parametros.get(i).getNombre());
			comboBox_GuardaResultado.addItem(parametros.get(i).getNombre());
		}
		for (int i = 0; i < variables.size(); i++) {
			cmbOperacion.addItem(variables.get(i).getNombre());
			comboBox_GuardaResultado.addItem(variables.get(i).getNombre());
		}
		contentPanel.add(cmbOperacion);
		
		chckbxGuardarResultado = new JCheckBox("Guardar en:");
		chckbxGuardarResultado.setBounds(12, 94, 165, 25);
		chckbxGuardarResultado.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (chckbxGuardarResultado.isSelected())
					comboBox_GuardaResultado.setEnabled(true);
				else
					comboBox_GuardaResultado.setEnabled(false);
			}
			
		});
		contentPanel.add(chckbxGuardarResultado);
		
		
		
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						accionOK();
					}
				});
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
			}
		}
	}
	
	private void accionOK() {
		//Retorno
		if (cmbOperacion.getSelectedItem().toString().contains("par_") || cmbOperacion.getSelectedItem().toString().contains("aux_")) {
			if (chckbxGuardarResultado.isSelected()) {
				resultado = comboBox_GuardaResultado.getSelectedItem().toString() + " = negacion(" + cmbOperacion.getSelectedItem().toString() + ")";
			} else {
				resultado = "negacion(" + cmbOperacion.getSelectedItem().toString() + ")";
			}
		} else {
			if (chckbxGuardarResultado.isSelected()) { //
				resultado = comboBox_GuardaResultado.getSelectedItem().toString() + " = negacion(" + llamarMetodo(cmbOperacion.getSelectedItem().toString()) + ")";
			} else {
				resultado = "negacion(" + llamarMetodo(cmbOperacion.getSelectedItem().toString()) + ")";
			}
		}
		// Comprueba si alguna de los metodos llamados se ha cancelado (se borra todo el proceso en este caso)
		if (resultado.contains("null"))
			resultado = null;
		dispose();
	}
	
	/**
	 * C�digo a ejecutar cuando es pulsado OK con una funci�n
	 */
	
	private String llamarMetodo(String metodo){
		String resultado = "";
		
		switch(metodo){
		case "Suma":
			VentanaSuma suma = new VentanaSuma(numParm, metodos, parametros, variables);
			suma.setVisible(true);
			resultado = suma.resultado;
			break;
			 
		case "Negaci�n":
			VentanaNegacion neg = new VentanaNegacion(numParm, metodos, parametros, variables);
			neg.setVisible(true);
			resultado = neg.resultado;
			break;
			
		case "Resta":
			VentanaResta resta = new VentanaResta(numParm, metodos, parametros, variables);
			resta.setVisible(true);
			resultado = resta.resultado;
			break;
			
		case "Potencia":
			VentanaPotencia potencia = new VentanaPotencia(numParm, metodos, parametros, variables);
			potencia.setVisible(true);
			resultado = potencia.resultado;
			break;
			
		case "Factorial":
			VentanaFactorial factorial = new VentanaFactorial(numParm, metodos, parametros, variables);
			factorial.setVisible(true);
			resultado = factorial.resultado;
			break;
			
		case "Media artim�tica":
			VentanaMediaAritmetica mediaAritmetica = new VentanaMediaAritmetica(numParm, metodos, parametros, variables);
			mediaAritmetica.setVisible(true);
			resultado = mediaAritmetica.resultado;
			break;
			
		case "Raiz cuadrada":
			VentanaRaizCuadrada raizCuadrada = new VentanaRaizCuadrada(numParm, metodos, parametros, variables);
			raizCuadrada.setVisible(true);
			resultado = raizCuadrada.resultado;
			break;
			
		case "Multiplicaci�n":
			VentanaMultiplicacion multiplicacion = new VentanaMultiplicacion(numParm, metodos, parametros, variables);
			multiplicacion.setVisible(true);
			resultado = multiplicacion.resultado;
			break;
			
		case "Porcentaje":
			VentanaPorcentaje porcentaje = new VentanaPorcentaje(numParm, metodos, parametros, variables);
			porcentaje.setVisible(true);
			resultado = porcentaje.resultado;
			break;
			
		case "Divisi�n":
			VentanaDivision division = new VentanaDivision(numParm, metodos, parametros, variables);
			division.setVisible(true);
			resultado = division.resultado;
			break;
			
		default:
			//System.out.println("No deberia haber llegado aqu�");
			break;

		}
		//System.out.println("Se ha ejecutado: " + resultado);
		//if (resultado == null)
		return resultado;
		//return null;
	}
}
