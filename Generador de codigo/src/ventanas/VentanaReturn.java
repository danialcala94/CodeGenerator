package ventanas;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import objetos.Parametro;
import objetos.Variable;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;

public class VentanaReturn extends JDialog {

	private final JPanel contentPanel = new JPanel();
	String resultado;

	/**
	 * Create the dialog.
	 * @param arrayVariables 
	 * @param arrayParametros 
	 */
	public VentanaReturn(ArrayList<Parametro> arrayParametros, ArrayList<Variable> arrayVariables) {
		setModal(true);
		setTitle("Return");
		setResizable(false);
		setBounds(100, 100, 282, 201);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblVariableQueSe = new JLabel("Variable que se devolver\u00E1:");
		lblVariableQueSe.setHorizontalAlignment(SwingConstants.CENTER);
		lblVariableQueSe.setBounds(10, 22, 246, 14);
		contentPanel.add(lblVariableQueSe);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(60, 69, 147, 20);
		contentPanel.add(comboBox);
		
		for(int i=0; i<arrayParametros.size(); i++){
			comboBox.addItem(arrayParametros.get(i).getNombre());
		}
		
		for(int i=0; i<arrayVariables.size(); i++){
			comboBox.addItem(arrayVariables.get(i).getNombre());
		}
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 131, 276, 33);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						resultado = "return "+comboBox.getSelectedItem().toString()+";\n";
						dispose();
					}
				});
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
			}
		}
	}
}
