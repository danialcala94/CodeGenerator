package ventanas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Panel;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;

import listeners.listenerGenerarCabecera;
import listeners.listenerGenerarMetodo;
import listeners.listenerbtnNegacion;
import listeners.listenerbtnRepet;
import listeners.listenerbtnSuma;
import objetos.Metodo;
import objetos.Parametro;
import objetos.Variable;

import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.TextArea;
import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Choice;
import javax.swing.JSpinner;
import javax.swing.JTextArea;

public class VentanaPrincipal {

	public JFrame getFrame() {
		return frame;
	}
	public JLabel getLblParametro() {
		return lblParametro;
	}
	public JLabel getLblNombreDelMetodo() {
		return lblNombreDelMetodo;
	}
	public JButton getBtnResta() {
		return btnResta;
	}
	public JButton getBtnMultiplicacion() {
		return btnMultiplicacion;
	}
	public JButton getBtnPotencia() {
		return btnPotencia;
	}
	public JButton getBtnMediaAritmetica() {
		return btnMediaAritmetica;
	}
	public JButton getBtnRaizCuadrada() {
		return btnRaizCuadrada;
	}
	public JButton getBtnPorcentaje() {
		return btnPorcentaje;
	}
	public JButton getBtnFactorial() {
		return btnFactorial;
	}
	public JButton getBtnDivision() {
		return btnDivision;
	}
	public JButton getBtnSuma() {
		return btnSuma;
	}
	public JTextField getTextField() {
		return textField;
	}
	public JComboBox getComboBox() {
		return comboBox;
	}
	public TextArea getTextArea() {
		return textArea;
	}
	public void setTextArea(TextArea text) {
		this.textArea = text;
	}

	private JFrame frame;
	private JLabel lblParametro;
	private JLabel lblNombreDelMetodo;
	private JButton btnResta = new JButton("Resta");
	private JButton btnMultiplicacion =new JButton("Multiplicacion");
	private JButton btnPotencia = new JButton("Potencia");
	private JButton btnMediaAritmetica = new JButton("Media aritmetica");
	private JButton btnRaizCuadrada = new JButton("Raiz cuadrada");
	private JButton btnPorcentaje = new JButton("Porcentaje");
	private JButton btnFactorial = new JButton("Factorial");
	private JButton btnDivision = new JButton("Division");
	private JButton btnSuma = new JButton("Suma");
	private JButton btnNegacion = new JButton("Negacion");
	private JButton btnRepetir = new JButton("Repeticion");
	private JButton btnCrearVariable = new JButton("Crear variable");
	JButton btnGenerarCabecera = new JButton("GENERAR CABECERA");
	JButton btnGenerarMetodo = new JButton("GENERAR M\u00C9TODO");
	JButton btnReturn = new JButton("Elegir return");
	private JTextField textField;
	private JComboBox comboBox;
	private TextArea textArea;
	
	// M�todos para gestionar cu�les est�n implementados, cu�les no, qu� nombre tienen, si son originales, qu� codigo tienen...
	private ArrayList<Metodo> arrayMetodos = new ArrayList<Metodo>();
	private ArrayList<Parametro> arrayParametros = new ArrayList<Parametro>();
	private ArrayList<Variable> arrayVariables = new ArrayList<Variable>();
	private int numParametros = 0;
	private String codigoAImplementar = "";
	
	listenerbtnNegacion lbn;
	
	public void setCodigoAImplementar(String codigoAImplementar) {
		this.codigoAImplementar = codigoAImplementar;
	}
	public void appendCodigoAImplementar(String codigoAImplementar) {
		// Habr� que ver c�mo gestionar los \n, los \t o los (;)
		this.codigoAImplementar += codigoAImplementar;
	}
	public String getCodigoAImplmentar() {
		return this.codigoAImplementar;
	}
	
	public ArrayList<Parametro> getArrayParametros(){
		return this.arrayParametros;
	}
	
	public ArrayList<Metodo> getArrayMetodos() {
		return this.arrayMetodos;
	}
	public void setArrayMetodos(ArrayList<Metodo> arrayMetodos) {
		this.arrayMetodos = arrayMetodos;
	}
	
	public ArrayList<Variable> getArrayVariable(){
		return this.arrayVariables;
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//System.out.println("Aplicaci�n para la asignatura de Ingenieria del Conocimiento\nGrado en Ingenieria Inform�tica (ULE)\nCurso 2017-2018\nAplicaci�n desarrollada integramente por:\n\tDaniel Alcal� Valera\n\tMart�n Bay�n Guti�rrez\n\tBeatriz Crespo Torbado");
					////System.out.println("INICIANDO APLICACI�N");
					VentanaPrincipal window = new VentanaPrincipal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public void inicializaComboBox(JComboBox combo) {
		for(int i=0; i<arrayMetodos.size(); i++) {
			if(!arrayMetodos.get(i).isOriginal()) {
				combo.addItem(arrayMetodos.get(i).getNombre().toString());
			}
		}
	}
	
	/**
	 * Create the application.
	 */
	public VentanaPrincipal() {
		initialize();
		this.frame.setIconImage(new ImageIcon("recursos/programacion.png").getImage());
	}
	
	/**
	 * Crea los 3+8 m�todos necesarios para la aplicaci�n. Tres m�todos utilizables desde el principio y ocho programables por el usuario.
	 */
	public void inicializarMetodos() {
		ArrayList<Metodo> metodos = new ArrayList<Metodo>();
		// isOriginal: true indica que son originales (se pueden utilizar desde el principio y ya est�n codificados desde entonces)
		// isOriginal: false indica que no son originales (no se pueden utilizar desde el principio ni est�n codificados desde entonces)
		// numParametros: > 0 indica que est�n activados (pueden ser o no originales)
		
		// M�todos que el programador ya ha codificado (creado)
		metodos.add(new Metodo("Suma", 2, "par_0 + par_1", true));
		metodos.add(new Metodo("Negaci�n", 1, "-par_0", true));
		metodos.add(new Metodo("Repetici�n", 2, "float aux = 0;\n for (int i = 0; i < par_1; i++) {\n \taux += par_0;\n };", true));
		
		// M�todos que el usuario debe codificar (crear)
		metodos.add(new Metodo("Resta"));
		metodos.add(new Metodo("Potencia"));
		metodos.add(new Metodo("Factorial"));
		metodos.add(new Metodo("Media aritm�tica"));
		metodos.add(new Metodo("Raiz cuadrada"));
		metodos.add(new Metodo("Multiplicaci�n"));
		metodos.add(new Metodo("Porcentaje"));
		metodos.add(new Metodo("Divisi�n"));
		
		this.arrayMetodos = metodos;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// Al iniciar, se instancian los 11 m�todos
		inicializarMetodos();
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 700, 761);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		comboBox = new JComboBox();
		inicializaComboBox(comboBox);
		comboBox.setBounds(54, 32, 310, 20);
		frame.getContentPane().add(comboBox);
		
		lblParametro = new JLabel("N\u00FAmero de par\u00E1metros");
		lblParametro.setBounds(438, 10, 140, 14);
		lblParametro.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblParametro);
		
		lblNombreDelMetodo = new JLabel("Nombre del m\u00E9todo");
		lblNombreDelMetodo.setBounds(54, 10, 310, 14);
		lblNombreDelMetodo.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblNombreDelMetodo);
		
		
		// BOTONES Y LISTENERS DE M�TODOS
		// Suma
		btnSuma.setEnabled(false);
		btnSuma.setBounds(10, 63, 185, 23);
		listenerbtnSuma lbs = new listeners.listenerbtnSuma(this);
		btnSuma.addActionListener(lbs);
		// Comprobar que guarde el texto solo si interesa, y a�ada ; y \n
		frame.getContentPane().add(btnSuma);
		
		// Repetici�n
		btnRepetir.setEnabled(false);
		btnRepetir.setBounds(266, 63, 150, 23);
		listenerbtnRepet lbr = new listeners.listenerbtnRepet(this);
		btnRepetir.addActionListener(lbr);
		// Comprobar que guarde el texto solo si interesa, y a�ada ; y \n
		frame.getContentPane().add(btnRepetir);
		
		// Negaci�n
		btnNegacion.setEnabled(false);
		btnNegacion.setBounds(489, 63, 185, 23);
		lbn = new listeners.listenerbtnNegacion(this, arrayMetodos, arrayParametros, arrayVariables);
		btnNegacion.addActionListener(lbn);
		// Comprobar que guarde el texto solo si interesa, y a�ada ; y \n
		frame.getContentPane().add(btnNegacion);
		
		// Resta
		btnResta.setBounds(10, 117, 150, 23);
		//TODO
		//Esto es solo para pruebas, deberia ser false
		btnResta.setEnabled(false);
		frame.getContentPane().add(btnResta);
		btnResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//TODO
				//Cambiar el constructor para que pase el numero de parametros indicado en el campo de numero de argumentos
				VentanaResta resta = new VentanaResta(numParametros, arrayMetodos, arrayParametros, arrayVariables);
				resta.show();
				resta.setModal(true);
				if(resta.resultado != null) {
					// A�ade el c�digo solo si no se ha cancelado en ning�n momento, y se a�ade ; y salto de l�nea
					textArea.setText(textArea.getText() + resta.resultado + ";\n");
				}
			}
		});
		
		// Multiplicaci�n
		btnMultiplicacion.setBounds(177, 175, 150, 23);
		btnMultiplicacion.setEnabled(false);
		frame.getContentPane().add(btnMultiplicacion);
		btnMultiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VentanaMultiplicacion multiplicacion = new VentanaMultiplicacion(Integer.parseInt(textField.getText().toString()), arrayMetodos, arrayParametros, arrayVariables);
				multiplicacion.setVisible(true);
				multiplicacion.setModalityType(ModalityType.APPLICATION_MODAL);
				if(multiplicacion.resultado!=null) {
					// A�ade el c�digo solo si no se ha cancelado en ning�n momento, y se a�ade ; y salto de l�nea
					textArea.setText(textArea.getText() + multiplicacion.resultado + ";\n");
				}
			}
		});
		
		// Potencia
		btnPotencia.setBounds(177, 117, 150, 23);
		btnPotencia.setEnabled(false);
		frame.getContentPane().add(btnPotencia);
		btnPotencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VentanaPotencia potencia = new VentanaPotencia(Integer.parseInt(textField.getText().toString()), arrayMetodos, arrayParametros, arrayVariables);
				potencia.setVisible(true);
				potencia.setModalityType(ModalityType.APPLICATION_MODAL);
				if(potencia.resultado!=null) {
					// A�ade el c�digo solo si no se ha cancelado en ning�n momento, y se a�ade ; y salto de l�nea
					textArea.setText(textArea.getText() + potencia.resultado + ";\n");
				}
			}
		});
		
		// Media aritm�tica
		btnMediaAritmetica.setBounds(524, 117, 150, 23);
		btnMediaAritmetica.setEnabled(false);
		frame.getContentPane().add(btnMediaAritmetica);
		btnMediaAritmetica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VentanaMediaAritmetica mediaAritmetica = new VentanaMediaAritmetica(Integer.parseInt(textField.getText().toString()), arrayMetodos, arrayParametros, arrayVariables);
				mediaAritmetica.setVisible(true);
				mediaAritmetica.setModalityType(ModalityType.APPLICATION_MODAL);
				if(mediaAritmetica.resultado!=null) {
					// A�ade el c�digo solo si no se ha cancelado en ning�n momento, y se a�ade ; y salto de l�nea
					textArea.setText(textArea.getText() + mediaAritmetica.resultado + ";\n");
				}
			}
		});
		
		// Raiz cuadrada
		btnRaizCuadrada.setBounds(10, 175, 150, 23);
		btnRaizCuadrada.setEnabled(false);
		frame.getContentPane().add(btnRaizCuadrada);
		btnRaizCuadrada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VentanaRaizCuadrada raizCuadrada = new VentanaRaizCuadrada(Integer.parseInt(textField.getText().toString()), arrayMetodos, arrayParametros, arrayVariables);
				raizCuadrada.setVisible(true);
				raizCuadrada.setModalityType(ModalityType.APPLICATION_MODAL);
				if(raizCuadrada.resultado!=null) {
					// A�ade el c�digo solo si no se ha cancelado en ning�n momento, y se a�ade ; y salto de l�nea
					textArea.setText(textArea.getText() + raizCuadrada.resultado + ";\n");
				}
			}
		});
		
		// Porcentaje
		btnPorcentaje.setBounds(349, 175, 150, 23);
		btnPorcentaje.setEnabled(false);
		frame.getContentPane().add(btnPorcentaje);
		btnPorcentaje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VentanaPorcentaje porcentaje = new VentanaPorcentaje(Integer.parseInt(textField.getText().toString()), arrayMetodos, arrayParametros, arrayVariables);
				porcentaje.setVisible(true);
				porcentaje.setModalityType(ModalityType.APPLICATION_MODAL);
				if(porcentaje.resultado!=null) {
					// A�ade el c�digo solo si no se ha cancelado en ning�n momento, y se a�ade ; y salto de l�nea
					textArea.setText(textArea.getText() + porcentaje.resultado + ";\n");
				}
			}
		});
		
		// Factorial
		btnFactorial.setBounds(349, 117, 150, 23);
		btnFactorial.setEnabled(false);
		frame.getContentPane().add(btnFactorial);
		btnFactorial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VentanaFactorial factorial = new VentanaFactorial(Integer.parseInt(textField.getText().toString()), arrayMetodos, arrayParametros, arrayVariables);
				factorial.setVisible(true);
				factorial.setModalityType(ModalityType.APPLICATION_MODAL);
				if(factorial.resultado!=null) {
					// A�ade el c�digo solo si no se ha cancelado en ning�n momento, y se a�ade ; y salto de l�nea
					textArea.setText(textArea.getText() + factorial.resultado + ";\n");
				}
			}
		});
		
		// Divisi�n
		btnDivision.setBounds(524, 175, 150, 23);
		btnDivision.setEnabled(false);
		frame.getContentPane().add(btnDivision);
		btnDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// TENEMOS QUE EMPEZAR A PASARLES EL arrayVariables PARA PODER HACER   Guardar en: variable
				VentanaDivision division = new VentanaDivision(Integer.parseInt(textField.getText().toString()), arrayMetodos, arrayParametros, arrayVariables);
				division.setVisible(true);
				division.setModalityType(ModalityType.APPLICATION_MODAL);
				if(division.resultado!=null) {
					// A�ade el c�digo solo si no se ha cancelado en ning�n momento, y se a�ade ; y salto de l�nea
					textArea.setText(textArea.getText() + division.resultado + ";\n");
				}
			}
		});
		// BOTONES Y LISTENERS DE M�TODOS
		

		// BOTONES Y LISTENERS DE CABECERA, CREAR VARIABLE, Y M�TODO
		listenerGenerarCabecera lgc = new listeners.listenerGenerarCabecera(this);
		btnGenerarCabecera.addActionListener(lgc);
		btnGenerarCabecera.setBounds(10, 224, 162, 38);
		frame.getContentPane().add(btnGenerarCabecera);
		btnGenerarCabecera.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				genCabecera();
			}
		});
		
		btnCrearVariable.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String valorVariable = JOptionPane.showInputDialog("Inicializar en:", 0);
				if (valorVariable != null) {
					float valor = 0;
					try {
						// Provoque la excepci�n si no es un n�mero
						valor = Float.parseFloat(valorVariable);
						if (valorVariable.length() > 9) {
							valorVariable = valorVariable.substring(0, 9);
							JOptionPane.showMessageDialog(null,
			                        "La variable solo admite 9 caracteres. Se utilizar� " + valorVariable,
			                        "Variable",
			                        JOptionPane.ERROR_MESSAGE);
						}
						// Coge el valor real, una vez reducido si hace falta
						valor = Float.parseFloat(valorVariable);
					} catch (Exception exc) {
						JOptionPane.showMessageDialog(null,
		                        "La variable debe ser un n�mero real. Se inicializar� autom�ticamente en 0.",
		                        "Variable",
		                        JOptionPane.ERROR_MESSAGE);
						valor = 0;
					}
					// Se instancia la nueva variable creada
					arrayVariables.add(new Variable());
					// y se a�ade al textArea
					textArea.setText(textArea.getText() + arrayVariables.get(arrayVariables.size() - 1).getTipo() + " " + arrayVariables.get(arrayVariables.size() - 1).getNombre() + " = (float) " + valor + ";\n");
				}
			}
			
		});
		btnCrearVariable.setBounds(187, 224, 140, 38);
		btnCrearVariable.setEnabled(false);
		frame.getContentPane().add(btnCrearVariable);
		
		
		listenerGenerarMetodo lgm = new listeners.listenerGenerarMetodo(this);
		btnGenerarMetodo.setEnabled(false);
		btnGenerarMetodo.setBounds(512, 224, 162, 38);
		frame.getContentPane().add(btnGenerarMetodo);
		btnGenerarMetodo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				////System.out.println("Se quiere crear un m�todo. Se debe comprobar que se haya introducido c�digo?");
				genMetodo();
			}
		});
		// BOTONES Y LISTENERS DE CABECERA Y M�TODO
		
		textField = new JTextField();
		textField.setBounds(438, 30, 140, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textArea = new TextArea("Aplicaci�n para la asignatura de Ingenieria del Conocimiento\nGrado en Ingenieria Inform�tica (ULE)\nCurso 2017-2018\nAplicaci�n desarrollada integramente por:\n\tDaniel Alcal� Valera\n\tMart�n Bay�n Guti�rrez\n\tBeatriz Crespo Torbado");
		textArea.setBounds(10, 304, 664, 408);
		textArea.setEditable(false);
		frame.getContentPane().add(textArea);
		
		//	Bot�n provisional para ver la evoluci�n. Al pulsarlo muestra por consola el estado de los m�todos.
		JButton btnAyuda = new JButton("Ayuda");
		btnAyuda.setToolTipText("Muestra, por consola, el estado de los distintos m\u00E9todos.");
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mostrarAyuda();
			}
		});
		btnAyuda.setBackground(new Color(255, 200, 0));
		btnAyuda.setBounds(290, 273, 100, 25);
		frame.getContentPane().add(btnAyuda);
		
		
		btnReturn.setEnabled(false);
		btnReturn.setBounds(349, 224, 140, 38);
		frame.getContentPane().add(btnReturn);
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonReturn();
			}
		});
		//	Bot�n provisional para ver la evoluci�n. Al pulsarlo muestra por consola el estado de los m�todos.
		
	}
		
	private boolean comprobarCabecera(String parametrosIntroducidos) {
		try	{
			numParametros = Integer.parseInt(parametrosIntroducidos);
			if (numParametros < 1 || numParametros > 10) {
				numParametros = 0;
				JOptionPane.showMessageDialog(null,
                        "El n�mero de par�metros debe estar comprendido entre 1 y 10.",
                        "Creaci�n de m�todo",
                        JOptionPane.ERROR_MESSAGE);
				this.textField.setText("");
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			numParametros = 0;
			JOptionPane.showMessageDialog(null,
                    "El n�mero de par�metros debe estar comprendido entre 1 y 10.",
                    "Creaci�n de m�todo",
                    JOptionPane.ERROR_MESSAGE);
			this.textField.setText("");
			return false;
		}
	}
	
	private void genCabecera() {
		this.textArea.setText("");
		boolean correcta = comprobarCabecera(textField.getText().toString());
		if (correcta) {
			////System.out.println("Generando cabecera con " + numParametros + " atributos.");
			// Mostrar como disponibles
			for (int i = 0; i < arrayMetodos.size(); i++) {
				if (arrayMetodos.get(i).isActivated()) {
					switch (arrayMetodos.get(i).getNombre()) {
					case "Suma":
						btnSuma.setEnabled(true);
						break;
					case "Negaci�n":
						btnNegacion.setEnabled(true);
						break;
					case "Repetici�n":
						btnRepetir.setEnabled(true);
						break;
					case "Resta":
						btnResta.setEnabled(true);
						break;
					case "Potencia":
						btnPotencia.setEnabled(true);
						break;
					case "Factorial":
						btnFactorial.setEnabled(true);
						break;
					case "Media aritm�tica":
						btnMediaAritmetica.setEnabled(true);
						break;
					case "Raiz cuadrada":
						btnRaizCuadrada.setEnabled(true);
						break;
					case "Multiplicaci�n":
						btnMultiplicacion.setEnabled(true);
						break;
					case "Porcentaje":
						btnPorcentaje.setEnabled(true);
						break;
					case "Divisi�n":
						btnDivision.setEnabled(true);
						break;
					}
				}
			}
			textField.setEditable(false);
			comboBox.setEnabled(false);
			btnGenerarCabecera.setEnabled(false);
			btnCrearVariable.setEnabled(true);
			btnReturn.setEnabled(true);
			String txtParametros = "(";
			for (int i = 0; i < numParametros; i++) {
				txtParametros += "float par_" + i;
				if (i != numParametros - 1)
					txtParametros += ", ";
				else
					txtParametros += ")";
			}
			// Modificamos el nombre del m�todo a mostrar en el textArea
			String nombreMetodo = comboBox.getSelectedItem().toString().toLowerCase();
			nombreMetodo = nombreMetodo.replace('�', 'a');
			nombreMetodo = nombreMetodo.replace('�', 'e');
			nombreMetodo = nombreMetodo.replace('�', 'i');
			nombreMetodo = nombreMetodo.replace('�', 'o');
			nombreMetodo = nombreMetodo.replace('�', 'u');
			nombreMetodo = nombreMetodo.replaceAll(" ", "_");
			
			textArea.setText("public static float " + nombreMetodo + txtParametros + "  {\n");
			// Generamos los par�metros autom�ticamente
			for (int i = 0; i < numParametros; i++) {
				arrayParametros.add(new Parametro());
				////System.out.println("Par�metro " + i + ": " + arrayParametros.get(i).getNombre());
			}
		} else { 
			////System.out.println("No se ha podido generar la cabecera por un error");
		}
	}
	
	private void genMetodo(){
		String metodoRealizado = comboBox.getSelectedItem().toString();
		for (int i = 0; i < arrayMetodos.size(); i++) {
			if (arrayMetodos.get(i).getNombre().equals(metodoRealizado)) {
				arrayMetodos.get(i).setNumParametros(numParametros);
				arrayMetodos.get(i).setParametros(arrayParametros);
				// Almacena cabecera y c�digo (no se necesita la cabecera sola para nada, as� que funciona as� bien
				String codigoEnTextArea = textArea.getText();
				/*for (int m = 0; m < codigoEnTextArea.length(); m++) {
					if (codigoEnTextArea.charAt(m) == '{') {
						codigoEnTextArea = codigoEnTextArea.substring(m, codigoEnTextArea.length() - m);
						break;
					}
				}*/
				// DEBE DEVOLVER UN VALOR (debe tener un   return algo;  )		(( IMPLEMENTAR: puede devolver un par�metro o una variable ))
				//  	pide valor a devolver => codigoEnTextArea += "return valorADevolver;\n"
				// <<OPCIONES>>
				////System.out.println("Elegir  return   de: ");
				for (int m = 0; m < arrayParametros.size(); m++) {
					////System.out.println(arrayParametros.get(m).getNombre());
				}
				for (int m = 0; m < arrayVariables.size(); m++) {
					////System.out.println(arrayVariables.get(m).getNombre());
				}
				// Simularemos el return. Hay que hacer ventana para que seleccione de las anteriores <<OPCIONES>>
				
				codigoEnTextArea += "}";
				arrayMetodos.get(i).setCodigo(codigoEnTextArea);
				codigoAImplementar = "";
				////System.out.println("Se ha generado el m�todo " + arrayMetodos.get(i).getNombre());
				for (int j = 0; j < comboBox.getItemCount(); j++) {
					if (arrayMetodos.get(i).getNombre().equalsIgnoreCase(comboBox.getItemAt(j).toString())) {
						comboBox.removeItemAt(j);
						break;
					}
				}
				desactivaBotones();
				////System.out.println("Se ha guardado el c�digo: \n" + arrayMetodos.get(i).getCodigo());
				// Hacer que no compruebe m�s una vez seteado el que es
				break;
			}
		}
		if (comboBox.getItemCount() > 0) {
			comboBox.setEnabled(true);
			btnGenerarCabecera.setEnabled(true);
			textField.setEditable(true);
		} else {
			JOptionPane.showMessageDialog(null,
                    "Ha implementado correctamente los 8 m�todos posibles.",
                    "Programa completado",
                    JOptionPane.INFORMATION_MESSAGE);
			////System.out.println("C�digo completado con �xito, no quedan m�todos que implementar.");
		}
		textField.setText("");
		
		// Vaciamos el array de par�metros
		arrayParametros = new ArrayList<Parametro>();
		// Vaciamos el array de variables
		arrayVariables = new ArrayList<Variable>();
		// Ponemos a 0 el contador interno para la generaci�n autom�tica de par�metros
		Parametro.vaciarParametros();
		// Ponemos a 0 el contador interno para la generaci�n autom�tica de variables
		Variable.vaciarVariables();
		btnGenerarMetodo.setEnabled(false);
		btnReturn.setEnabled(false);
		//textArea.setText(textArea.getText() + "\n}");
		// Se debe vaciar, al crearse ya se guarda en el c�digo del m�todo correspondiente.
		textArea.setText("");
		
		
	}
	
	/**
	 * Muestra, por consola, el estado de los distintos m�todos implementados.
	 */
	public void mostrarAyuda() {
		ventanaAyuda ayuda = new ventanaAyuda(arrayMetodos);
		ayuda.setVisible(true);
		ayuda.setModal(true);
	}
	
	
	public void botonReturn() {
		VentanaReturn ventReturn = new VentanaReturn(arrayParametros, arrayVariables);
		ventReturn.setVisible(true);
		ventReturn.setModal(true);
		String retorno = ventReturn.resultado;
		if(retorno != null) {
			textArea.setText(textArea.getText()+retorno);
			btnGenerarMetodo.setEnabled(true);
			desactivaBotones();
		}
	}
	
	public void desactivaBotones() {
		btnSuma.setEnabled(false);
		btnCrearVariable.setEnabled(false);
		btnDivision.setEnabled(false);
		btnResta.setEnabled(false);
		btnRepetir.setEnabled(false);
		btnMultiplicacion.setEnabled(false);
		btnMediaAritmetica.setEnabled(false);
		btnPorcentaje.setEnabled(false);
		btnNegacion.setEnabled(false);
		btnPotencia.setEnabled(false);
		btnPorcentaje.setEnabled(false);
		btnFactorial.setEnabled(false);
		btnRaizCuadrada.setEnabled(false);
		btnDivision.setEnabled(false);
		btnGenerarCabecera.setEnabled(false);
		btnCrearVariable.setEnabled(false);
		btnReturn.setEnabled(false);
	}
}
