package ventanas;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import objetos.Metodo;

import javax.swing.JLabel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ventanaCodigo extends JDialog {

	/**
	 * Create the dialog.
	 */
	public ventanaCodigo(Metodo metodo) {
		setModal(true);
		setTitle(metodo.getNombre());
		setBounds(100, 100, 402, 408);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
			}
		}
		{
			TextArea textArea = new TextArea();
			getContentPane().add(textArea, BorderLayout.CENTER);
			textArea.setText(metodo.getCodigo());
		}
	}
}