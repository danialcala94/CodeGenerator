package ventanas;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javafx.scene.control.ComboBox;
import objetos.Metodo;
import objetos.Parametro;
import objetos.Variable;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

public class VentanaSuma extends JDialog {

	private final JPanel contentPanel = new JPanel();
	

	/**
	 * Create the dialog.
	 */
	public String resultado;
	int numVariables;
	JComboBox comboBox;
	JComboBox comboBox_1;
	JComboBox comboBox_2 = new JComboBox(new Object[]{});
	private JTextField textField_num;
	JCheckBox chckbxGuardarResultado = new JCheckBox("Guardar resultado:");
	JRadioButton rdbtn_operacion = new JRadioButton("");
	JRadioButton rdbtn_numero = new JRadioButton("");
	ArrayList<Metodo> metodos;
	ArrayList<Parametro> parametros;
	ArrayList<Variable> variables;
	
	public VentanaSuma(int numVariables, ArrayList<Metodo> metodos, ArrayList<Parametro> parametros, ArrayList<Variable> variables) {
		this.setTitle("Suma");
		this.metodos = metodos;
		this.parametros = parametros;
		this.variables = variables;
		JButton okButton = new JButton("OK");
		okButton.setEnabled(false);
		this.numVariables = numVariables;
		this.setModal(true);
		setBounds(100, 100, 482, 257);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel label = new JLabel("");
			label.setBounds(217, 10, 0, 0);
			contentPanel.add(label);
		}
		comboBox = new JComboBox();
		comboBox.setBounds(10, 47, 190, 20);
		contentPanel.add(comboBox);

		
		comboBox_1 = new JComboBox();
		comboBox_1.setEnabled(false);
		comboBox_1.setBounds(266, 47, 190, 20);
		contentPanel.add(comboBox_1);
		
		comboBox_2 = new JComboBox(new Object[]{});
		comboBox_2.setEnabled(false);
		comboBox_2.setBounds(158, 154, 139, 20);
		contentPanel.add(comboBox_2);
		
		addMetodosActivados(comboBox);
		addMetodosActivados(comboBox_1);
		
		
		//A�ade los parametros a la lista de Opciones
		for(int i=0;i<parametros.size();i++){
			comboBox.addItem(parametros.get(i).getNombre());
			comboBox_1.addItem(parametros.get(i).getNombre());
			comboBox_2.addItem(parametros.get(i).getNombre());
		}
		
		for(int i=0;i<variables.size();i++) {
			comboBox.addItem(variables.get(i).getNombre());
			comboBox_1.addItem(variables.get(i).getNombre());
			comboBox_2.addItem(variables.get(i).getNombre());
		}
		
		JLabel lblParametro = new JLabel("Par\u00E1metro 1");
		lblParametro.setHorizontalAlignment(SwingConstants.CENTER);
		lblParametro.setBounds(10, 10, 190, 14);
		contentPanel.add(lblParametro);
		
		JLabel lblParametro_1 = new JLabel("Par\u00E1metro 2");
		lblParametro_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblParametro_1.setBounds(266, 10, 190, 14);
		contentPanel.add(lblParametro_1);
		
		ButtonGroup grupoBotones = new ButtonGroup();
		

		grupoBotones.add(rdbtn_operacion);
		rdbtn_operacion.setBounds(238, 47, 21, 23);
		contentPanel.add(rdbtn_operacion);
		rdbtn_operacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				okButton.setEnabled(true);
				comboBox_1.setEnabled(true);
				textField_num.setEnabled(false);
			}
		});
		
		textField_num = new JTextField();
		textField_num.setEnabled(false);
		textField_num.setBounds(266, 81, 190, 20);
		contentPanel.add(textField_num);
		textField_num.setColumns(10);
		
		grupoBotones.add(rdbtn_numero);
		rdbtn_numero.setBounds(238, 80, 21, 23);
		contentPanel.add(rdbtn_numero);
		rdbtn_numero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				okButton.setEnabled(true);
				comboBox_1.setEnabled(false);
				textField_num.setEnabled(true);
			}
		});
		
		chckbxGuardarResultado.setBounds(158, 124, 139, 23);
		contentPanel.add(chckbxGuardarResultado);
		chckbxGuardarResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(chckbxGuardarResultado.isSelected()) {
					comboBox_2.setEnabled(true);
				}else{
					comboBox_2.setEnabled(false);
				}
			}
		});
		
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						//Accion del boton OK
						calculaSuma();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//Accion al cancelar
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	
	public void calculaSuma() {
		String arg_1 = "";
		String arg_2 = "";
		String retorno = "";
		
		//Parametro 1
		if(comboBox.getSelectedItem().toString().contains("par_")  || comboBox.getSelectedItem().toString().contains("aux_")) {
			arg_1 = comboBox.getSelectedItem().toString();
		}else {
			arg_1 = llamarMetodo(comboBox.getSelectedItem().toString());
		}
		
		
		//Parametro 2
		if(rdbtn_numero.isSelected()) {
			try {
				 int numero = Integer.parseInt(this.textField_num.getText().toString());
				 arg_2 = ""+numero;
			}catch (Exception e) {
				JOptionPane.showMessageDialog(null,
	                    "Debe introducir un n�mero entero en el campo del parametro 2",
	                    "Valor invalido",
	                    JOptionPane.ERROR_MESSAGE);
				this.textField_num.setText("");
				return;
			}
		}else{
			if(comboBox_1.getSelectedItem().toString().contains("par_")  || comboBox_1.getSelectedItem().toString().contains("aux_")) {
				arg_2 = comboBox_1.getSelectedItem().toString();
			}else {
				arg_2 = llamarMetodo(comboBox_1.getSelectedItem().toString());
			}
		}
		
		
		//Retorno
		if(chckbxGuardarResultado.isSelected()) {
			retorno = comboBox_2.getSelectedItem().toString()+" = Suma("+arg_1+" , "+arg_2+")";
		}else {
			retorno = "Suma("+arg_1+" , "+arg_2+")";
		}
		// Si contiene null, alguna ventana de operaci�n se ha cerrado o cancelado, que no devuelva nada
		if(retorno.contains("null")) {
			this.resultado = null;
		}else{
			this.resultado = retorno;
		}
		dispose();
	}
	
	private String llamarMetodo(String metodo){
		String resultado = "";
		
		switch(metodo){
		case "Suma":
			VentanaSuma suma = new VentanaSuma(numVariables, metodos, parametros, variables);
			suma.setVisible(true);
			resultado = suma.resultado;
			break;
			 
		case "Negaci�n":
			VentanaNegacion neg = new VentanaNegacion(numVariables, metodos, parametros, variables);
			neg.setVisible(true);
			resultado = neg.resultado;
			break;
			
		case "Resta":
			VentanaResta resta = new VentanaResta(numVariables, metodos, parametros, variables);
			resta.setVisible(true);
			resultado = resta.resultado;
			break;
			
		case "Potencia":
			VentanaPotencia potencia = new VentanaPotencia(numVariables, metodos, parametros, variables);
			potencia.setVisible(true);
			resultado = potencia.resultado;
			break;
			
		case "Factorial":
			VentanaFactorial factorial = new VentanaFactorial(numVariables, metodos, parametros, variables);
			factorial.setVisible(true);
			resultado = factorial.resultado;
			break;
			
		case "Media artim�tica":
			VentanaMediaAritmetica mediaAritmetica = new VentanaMediaAritmetica(numVariables, metodos, parametros, variables);
			mediaAritmetica.setVisible(true);
			resultado = mediaAritmetica.resultado;
			break;
			
		case "Raiz cuadrada":
			VentanaRaizCuadrada raizCuadrada = new VentanaRaizCuadrada(numVariables, metodos, parametros, variables);
			raizCuadrada.setVisible(true);
			resultado = raizCuadrada.resultado;
			break;
			
		case "Multiplicaci�n":
			VentanaMultiplicacion multiplicacion = new VentanaMultiplicacion(numVariables, metodos, parametros, variables);
			multiplicacion.setVisible(true);
			resultado = multiplicacion.resultado;
			break;
			
		case "Porcentaje":
			VentanaPorcentaje porcentaje = new VentanaPorcentaje(numVariables, metodos, parametros, variables);
			porcentaje.setVisible(true);
			resultado = porcentaje.resultado;
			break;
			
		case "Divisi�n":
			VentanaDivision division = new VentanaDivision(numVariables, metodos, parametros, variables);
			division.setVisible(true);
			resultado = division.resultado;
			break;
			
		default:
			//System.out.println("No deberia haber llegado a aqu�");
			break;

		}
		return resultado;
	}
	
	private void addMetodosActivados(JComboBox comboBox) {
		for(int i=0; i<metodos.size(); i++) {
			if(metodos.get(i).isActivated()) {
				comboBox.addItem(metodos.get(i).getNombre());
			}
		}
	}
}


