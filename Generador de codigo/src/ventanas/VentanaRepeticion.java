package ventanas;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import objetos.Metodo;
import objetos.Parametro;
import objetos.Variable;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollBar;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;

public class VentanaRepeticion extends JDialog {

	private final JPanel contentPanel = new JPanel();
	public String numRepeticiones;
	public String repeticion = "";
	public String codigo = "";
	public ArrayList<Metodo> metodos = new ArrayList<Metodo>();
	public ArrayList<Parametro> parametros = new ArrayList<Parametro>();
	public ArrayList<Variable> arrayVariables = new ArrayList<Variable>();
	int numParam;
	JCheckBox chckbxOperacion1 = new JCheckBox("Operaci�n1");
	JCheckBox chckbxOperacion2 = new JCheckBox("Operaci�n2");
	JRadioButton rdbtnNewRadioButtonFOR = new JRadioButton("Numero de repeticiones");
	JRadioButton rdbtnRepetirMientrasWHILE = new JRadioButton("Repetir mientras ");
	JButton okButton = new JButton("OK");

	/**
	 * Create the dialog.
	 */
	public VentanaRepeticion(int numPArametros, ArrayList<Metodo> metodos, ArrayList<Parametro> parametros, ArrayList<Variable> variables) {
		this.metodos = metodos;
		this.parametros = parametros;
		this.numParam = numPArametros;
		this.arrayVariables = variables;
		okButton.setEnabled(false);
		setModal(true);
		setTitle("Repeticion");
		setBounds(100, 100, 481, 327);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		
		
		JComboBox comboBoxOperacion1 = new JComboBox();
		addMetodosActivados(comboBoxOperacion1);
		comboBoxOperacion1.setEnabled(false);
		comboBoxOperacion1.setBounds(120, 186, 93, 22);
		contentPanel.add(comboBoxOperacion1);
		

		JComboBox comboBoxOperacion2 = new JComboBox();
		addMetodosActivados(comboBoxOperacion2);
		comboBoxOperacion2.setBounds(349, 186, 102, 22);
		comboBoxOperacion2.setEnabled(false);
		contentPanel.add(comboBoxOperacion2);
		
		
		JLabel lblIncrementosDe = new JLabel("Incrementos de");
		lblIncrementosDe.setBounds(67, 80, 102, 16);
		contentPanel.add(lblIncrementosDe);
		
		JSpinner spinnerNumIncre = new JSpinner();
		spinnerNumIncre.setEnabled(false);
		spinnerNumIncre.setModel(new SpinnerNumberModel(1, 1, 10, 1));
		spinnerNumIncre.setBounds(235, 70, 37, 22);
		contentPanel.add(spinnerNumIncre);
		
		JComboBox comboBoxFor = new JComboBox();
		comboBoxFor.setEnabled(false);
		comboBoxFor.setBounds(235, 35, 148, 22);
		contentPanel.add(comboBoxFor);
		
		JComboBox valor1While = new JComboBox();
		valor1While.setEnabled(false);
		//valor1While.addItem("i");
		valor1While.setBounds(157, 118, 60, 22);
		contentPanel.add(valor1While);
		
		String [] signos = {"mayor que", "mayor o igual que", "menor que", "menor o igual que", "igual que","distinto que"};
		JComboBox signoWhile = new JComboBox(signos);
		signoWhile.setEnabled(false);
		signoWhile.setBounds(229, 118, 150, 22);
		contentPanel.add(signoWhile);
		

		chckbxOperacion1.setBounds(14, 185, 93, 25);
		contentPanel.add(chckbxOperacion1);
		chckbxOperacion1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				compruebaActivacionOk();
				if(chckbxOperacion1.isSelected()){
					comboBoxOperacion1.setEnabled(true);
				}else{
					comboBoxOperacion1.setEnabled(false);
				}
			}
		});
		
		chckbxOperacion2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				compruebaActivacionOk();	
				if(chckbxOperacion2.isSelected()){
					comboBoxOperacion2.setEnabled(true);
				}else{
					comboBoxOperacion2.setEnabled(false);
				}
			}
		});
		
	
		chckbxOperacion2.setBounds(248, 185, 93, 25);
		contentPanel.add(chckbxOperacion2);
		
		JComboBox valor2While = new JComboBox();
		valor2While.setEnabled(false);
		valor2While.setBounds(391, 118, 60, 22);
		/*
		valor2While.addItem("i");
		valor2While.addItem("True");
		valor2While.addItem("False");
		*/
		contentPanel.add(valor2While);
		
		ButtonGroup grupoBtns = new ButtonGroup();
		

		grupoBtns.add(rdbtnNewRadioButtonFOR);
		rdbtnNewRadioButtonFOR.setBounds(14, 36, 189, 25);
		contentPanel.add(rdbtnNewRadioButtonFOR);
		rdbtnNewRadioButtonFOR.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(rdbtnNewRadioButtonFOR.isSelected()){
					//System.out.println("Haremos un FOR!!\n");
					comboBoxFor.setEnabled(true);
					spinnerNumIncre.setEnabled(true);
					
					valor1While.setEnabled(false);
					signoWhile.setEnabled(false);
					valor2While.setEnabled(false);
					
					compruebaActivacionOk();
					
					
				}
			}
		
		});
		

		grupoBtns.add(rdbtnRepetirMientrasWHILE);
		rdbtnRepetirMientrasWHILE.setBounds(14, 117, 127, 25);
		contentPanel.add(rdbtnRepetirMientrasWHILE);
		
		
		rdbtnRepetirMientrasWHILE.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(rdbtnRepetirMientrasWHILE.isSelected()){
					//System.out.println("Haremos un WHILE!!\n");
					valor1While.setEnabled(true);
					signoWhile.setEnabled(true);
					valor2While.setEnabled(true);
					
					comboBoxFor.setEditable(false);
					comboBoxFor.setEnabled(false);
					spinnerNumIncre.setEnabled(false);
					
					compruebaActivacionOk();
					
				}
			}
		});
		
		
		//A�adir a los comboBox los parametros y las variables.
		
		for(int j=0; j<arrayVariables.size();j++){
			valor1While.addItem(arrayVariables.get(j).getNombre());
			valor2While.addItem(arrayVariables.get(j).getNombre());
			comboBoxFor.addItem(arrayVariables.get(j).getNombre());
		}
		
		
		for(int k = 0; k<parametros.size();k++){
			valor1While.addItem(parametros.get(k).getNombre());
			valor2While.addItem(parametros.get(k).getNombre());
			comboBoxFor.addItem(parametros.get(k).getNombre());
		}
		
		
		comboBoxFor.addItem("Suma");
		
	

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{

				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String numRep = comboBoxFor.getSelectedItem().toString();
						String metodoRepetido = "";
						String valorIncremento = spinnerNumIncre.getValue().toString();

						if(rdbtnNewRadioButtonFOR.isSelected()){
							if(numRep.equals("Suma")){
								ventanas.VentanaSuma ventSum = new ventanas.VentanaSuma(numPArametros, metodos, parametros, arrayVariables);
								ventSum.show();
								ventSum.setModal(true);
								numRep = ventSum.resultado;
							}
							
							repeticion = "for(int i = 0;i<"+numRep+";i=i+"+valorIncremento+"){\n\t";
							
						}else if(rdbtnRepetirMientrasWHILE.isSelected()){
							String signoFinal = "";
							String [] signosAux = {">", ">=", "<", "<=","==","!="};
							String valor1Whi = valor1While.getSelectedItem().toString();
							String valorSignoAux = signoWhile.getSelectedItem().toString();
							String valor2Whi = valor2While.getSelectedItem().toString();
							//System.out.println(valor1Whi+ "\n" +valorSignoAux+"\n"+valor2Whi+"\n");
							for(int i = 0;i<signos.length;i++){
								if(signos[i].equals(valorSignoAux)){
									signoFinal = signosAux[i];
								}
							}		
							repeticion = "while("+valor1Whi+" "+signoFinal+" "+valor2Whi+"){\n\t";	
							
						}
								
						if (numRep != null) {
							String codigo1 = "";
							String codigo2 = "";
							if(chckbxOperacion1.isSelected()){
								codigo1 = llamarMetodo(comboBoxOperacion1.getSelectedItem().toString())+";\n\t";
							}
							
							if(chckbxOperacion2.isSelected() && codigo1 != null){
								codigo2 = llamarMetodo(comboBoxOperacion2.getSelectedItem().toString())+";\n";	
							}
						
							numRepeticiones = comboBoxFor.getSelectedItem().toString();
							if (codigo1 != null && codigo2 != null)
								repeticion = repeticion + codigo1 + codigo2 + "}\n";
							else
								repeticion = null;
						} else
							repeticion = null;
						//System.out.println("\n\n"+repeticion+"\n\n");
						dispose();
					}

				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);


			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	
	private void addMetodosActivados(JComboBox comboBox) {
		for(int i=0; i<metodos.size(); i++) {
			if(metodos.get(i).isActivated() && !metodos.get(i).getNombre().equals("Repetici�n")) {
				comboBox.addItem(metodos.get(i).getNombre());
			}
		}
	}
	
	
	private String llamarMetodo(String metodo){
		String resultado = "";
		
		switch(metodo){
		case "Suma":
			VentanaSuma suma = new VentanaSuma(this.numParam, metodos, parametros, arrayVariables);
			suma.setVisible(true);
			resultado = suma.resultado;
			break;
			 
		case "Negaci�n":
			VentanaNegacion neg = new VentanaNegacion(this.numParam, metodos, parametros, arrayVariables);
			neg.setVisible(true);
			resultado = neg.resultado;
			break;
			
		case "Resta":
			VentanaResta resta = new VentanaResta(this.numParam, metodos, parametros, arrayVariables);
			resta.setVisible(true);
			resultado = resta.resultado;
			break;
			
		case "Potencia":
			VentanaPotencia potencia = new VentanaPotencia(this.numParam, metodos, parametros, arrayVariables);
			potencia.setVisible(true);
			resultado = potencia.resultado;
			break;
			
		case "Factorial":
			VentanaFactorial factorial = new VentanaFactorial(this.numParam, metodos, parametros, arrayVariables);
			factorial.setVisible(true);
			resultado = factorial.resultado;
			break;
			
		case "Media artim�tica":
			VentanaMediaAritmetica mediaAritmetica = new VentanaMediaAritmetica(this.numParam, metodos, parametros, arrayVariables);
			mediaAritmetica.setVisible(true);
			resultado = mediaAritmetica.resultado;
			break;
			
		case "Raiz cuadrada":
			VentanaRaizCuadrada raizCuadrada = new VentanaRaizCuadrada(this.numParam, metodos, parametros, arrayVariables);
			raizCuadrada.setVisible(true);
			resultado = raizCuadrada.resultado;
			break;
			
		case "Multiplicaci�n":
			VentanaMultiplicacion multiplicacion = new VentanaMultiplicacion(this.numParam, metodos, parametros, arrayVariables);
			multiplicacion.setVisible(true);
			resultado = multiplicacion.resultado;
			break;
			
		case "Porcentaje":
			VentanaPorcentaje porcentaje = new VentanaPorcentaje(this.numParam, metodos, parametros, arrayVariables);
			porcentaje.setVisible(true);
			resultado = porcentaje.resultado;
			break;
			
		case "Divisi�n":
			VentanaDivision division = new VentanaDivision(this.numParam, metodos, parametros, arrayVariables);
			division.setVisible(true);
			resultado = division.resultado;
			break;
			
		default:
			//System.out.println("No deberia haber llegado a aqu�");
			break;

		}
		return resultado;
	}
	
	public void compruebaActivacionOk(){
		if((chckbxOperacion1.isSelected() || chckbxOperacion2.isSelected()) && (rdbtnNewRadioButtonFOR.isSelected() || rdbtnRepetirMientrasWHILE.isSelected())){
			okButton.setEnabled(true);
		}else{
			okButton.setEnabled(false);
		}
	}
}
